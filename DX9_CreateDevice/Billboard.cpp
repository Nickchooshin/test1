#include "Billboard.h"

CBillboard::CBillboard(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice = pd3dDevice;
	m_pVB = NULL;
	m_pIB = NULL;
	m_pTexture = NULL;
}

CBillboard::~CBillboard()
{
	SAFE_RELEASE(m_pd3dDevice);
	SAFE_RELEASE(m_pVB);
	SAFE_RELEASE(m_pIB);
	SAFE_RELEASE(m_pTexture);
}

HRESULT CBillboard::Initailize()
{
	if(FAILED(InitVB()))			{ return E_FAIL; }
	if(FAILED(InitIB()))				{ return E_FAIL; }
	if(FAILED(InitTexture()))	{ return E_FAIL; }

	return S_OK;
}

HRESULT CBillboard::InitVB()
{
	BILLBOARDVERTEX vertices[] = 
    {
        { -1.0f,-1.0f, -1.0f, 0.0f, 1.0f},   // v0
        { -1.0f,	1.0f, -1.0f, 0.0f, 0.0f},   // v1
        {  1.0f,  1.0f, -1.0f, 1.0f, 0.0f},   // v2
        {  1.0f, -1.0f, -1.0f, 1.0f, 1.0f},   // v3
    };
 
	if(FAILED(m_pd3dDevice->CreateVertexBuffer(4*sizeof(BILLBOARDVERTEX), 0, D3DFMT_BILLBOARDVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)))
    {
        return E_FAIL;
    }
 
    VOID* pVertices;
    if(FAILED(m_pVB->Lock(0, sizeof(vertices), (void**)&pVertices, 0)))
        return E_FAIL;
    memcpy(pVertices, vertices, sizeof(vertices));
    m_pVB->Unlock();
	
	return S_OK;
}

HRESULT CBillboard::InitIB()
{
	BILLBOARDINDEX indices[] = 
    {
        {0, 1, 2}, 
		{0, 2, 3}    
	};
 
	if(FAILED(m_pd3dDevice->CreateIndexBuffer(2*sizeof(BILLBOARDINDEX), 0, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_pIB, NULL)))
    {
        return E_FAIL;
    }
 
    VOID* pIndeces;
    if(FAILED(m_pIB->Lock(0, sizeof(indices), (void**)&pIndeces, 0)))
        return E_FAIL;
    memcpy(pIndeces, indices, sizeof(indices));
    m_pIB->Unlock();

	return S_OK;
}

HRESULT CBillboard::InitTexture()
{
	if(FAILED(D3DXCreateTextureFromFile(m_pd3dDevice, "./Resource/tree01S.dds", &m_pTexture))) {
		MessageBox(NULL, "이미지 파일 없음", "실패", MB_OK);
		return E_FAIL;
	}
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

	return S_OK;
}

VOID CBillboard::Draw()
{
	D3DXMATRIXA16 matWorld, matTrans, matScale ;
	D3DXMatrixIdentity(&matWorld) ;
	D3DXMatrixTranslation(&matTrans, 2, 1, 5);
	D3DXMatrixScaling(&matScale, 20, 20, 20);
	matWorld = matWorld * matTrans * matScale ;
	m_pd3dDevice->SetTransform(D3DTS_WORLD, &matWorld);

	// Alpha
	m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	//
	m_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// Drawong
	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(BILLBOARDVERTEX));		// 버텍스 데이터  
	m_pd3dDevice->SetFVF(D3DFMT_BILLBOARDVERTEX);										// 버텍스 정보 
	m_pd3dDevice->SetIndices(m_pIB);																		// 인덱스 정보	
	m_pd3dDevice->SetTexture(0, m_pTexture);														// 텍스쳐 정보
	m_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);		// 렌더링B

	m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}










