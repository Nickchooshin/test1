#ifndef _SPACESHIP_H_
#define _SPACESHIP_H_

#include "Common.h"

class CSpaceship
{
private:
	LPDIRECT3DDEVICE9							m_pd3dDevice;		// 렌더링에 사용될 D3D 디바이스
	LPDIRECT3DVERTEXBUFFER9				m_pVB;						// 정점을 보관할 정점 버퍼 
	LPDIRECT3DINDEXBUFFER9				m_pIB;						// 정점 인덱스를 보관할 버퍼

	SPACESHIPVERTEX									m_pVertices[16];							// 재배열된 버텍스 버퍼
	SPACESHIPINDEX									m_pIndices[10];							// 인덱스 버퍼

public:														
	CSpaceship(LPDIRECT3DDEVICE9 pd3dDevice);					// 오버로딩 생성자
	~CSpaceship();																				// 소멸자

	HRESULT Initailize();
	HRESULT InitVB();
	HRESULT InitIB();

	VOID Draw();
};

#endif 