#include "PICKING.h"
#include <stdio.h>

CPICKING::CPICKING(void)
{
}

CPICKING::CPICKING(LPDIRECT3DDEVICE9 pD3dDevice)
{
	m_pD3dDevice	 = pD3dDevice;

	m_pVB			 = NULL;
	m_pTexture		 = NULL;

	D3DXMatrixIdentity(&m_Matrix);

	InitVB();
	InitTexture();
}

CPICKING::~CPICKING(void)
{
	SAFE_RELEASE(m_pVB);
	SAFE_RELEASE(m_pTexture);
}

HRESULT CPICKING::InitVB()
{
	PICKINGVERTEX vertices[] = 
	{
		// 바닥
		{ D3DXVECTOR3(-0.5f, 0.0f,  0.5f), 0.0f, 1.0f },// P2
		{ D3DXVECTOR3( 0.5f, 0.0f,  0.5f), 0.0f, 0.0f }, // P0
		{ D3DXVECTOR3(-0.5f, 0.0f, -0.5f), 1.0f, 0.0f }, // P1
		{ D3DXVECTOR3( 0.5f, 0.0f, -0.5f), 1.0f, 1.0f } // P3
	};


	if(FAILED(m_pD3dDevice->CreateVertexBuffer(
		4*sizeof(PICKINGVERTEX),	// 버텍스의 크기
		0,							// 버퍼 사용 형식
		D3DFVF_PICKINGVERTEX,		// 버텍스 버퍼의 기술
		D3DPOOL_DEFAULT,			// 메모리 풀
		&m_pVB,						// 버텍스 버퍼를 가리키는 포인터
		NULL)))   
	{
		return E_FAIL;
	}

	VOID* pCVertices;
	if(FAILED(m_pVB->Lock(0, sizeof(vertices), (void**)&pCVertices, 0)))
		return E_FAIL;
	memcpy(pCVertices, vertices, sizeof(vertices));
	m_pVB->Unlock();

	return S_OK;
}

HRESULT CPICKING::InitTexture()
{
	if(FAILED(D3DXCreateTextureFromFile(m_pD3dDevice, "woodfloor.bmp", &m_pTexture)))
	{
		return E_FAIL;
	}

	m_pD3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pD3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pD3dDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

	return S_OK;
}

VOID CPICKING::Update()
{
	D3DXVECTOR3 v ;
	float dist = .0f ;
	Sphere sphere(D3DXVECTOR3(.0f, .0f, .0f), 10.0f) ;
	if(IntersectSphere(sphere, v, dist))
	{
		Draw() ;
	}
}

VOID CPICKING::Draw()
{
	//맵 텍스쳐 세팅
	D3DXMATRIX I;
	D3DXMatrixIdentity(&I);
	m_pD3dDevice->SetTransform(D3DTS_WORLD, &I);
	m_pD3dDevice->SetTexture(0, m_pTexture); 
	m_pD3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(PICKINGVERTEX));
	m_pD3dDevice->SetFVF(D3DFVF_PICKINGVERTEX);

	//맵 그리기
	for(int i = 0 ; i < 10 ; i++)
	{
		for(int j = 0 ; j < 10 ; j++)
		{
			// 현재 그려지고 있는 정점에 마우스 픽킹이 안되었을 경우만 그림
			if(!((int)(m_PickingPos.x+1.0f) == i && (int)(m_PickingPos.z+1.0f) == j)) 
			{
				printf("-------------------------------------------\n");
				printf("[x:%d], [y:%d]",i, j);
				printf("[mousex:%d,mousez:%d\n", (int)(m_PickingPos.x+1), (int)(m_PickingPos.z+1));

				I._41 = (float)i;
				I._43 = (float)j;
				m_pD3dDevice->SetTransform(D3DTS_WORLD, &I);
				m_pD3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
			}
			else
			{
				printf("\nPicking...\n");
			}
			
		}
	}
}

// 투영창의 포인트(px, py)점을 획득
// 원점과 투영창의 포인트 점을 통과하는 광선 구함. 
RAY CPICKING::CalcPickingRay(int x, int y)
{
	// 여기를 코딩하세요.  
	float px = 0.0f ;
	float py = 0.0f ;

	D3DVIEWPORT9 vp ;
	m_pD3dDevice->GetViewport(&vp) ;

	D3DXMATRIX proj ;
	m_pD3dDevice->GetTransform(D3DTS_PROJECTION, &proj) ;

	px = ((( 2.0f*x) / vp.Width) - 1.0f) / proj(0, 0) ;
	py = (((-2.0f*y) / vp.Height) + 1.0f) / proj(1, 1) ;

	RAY ray ;
	ray.origin = D3DXVECTOR3(0.0f, 0.0f, 0.0f) ;
	ray.direction = D3DXVECTOR3(px, py, 1.0f) ;

	return ray ;
}


VOID CPICKING::TransformRay(RAY* ray, D3DXMATRIX* T)
{
	// transform the ray's origin, w = 1.
	D3DXVec3TransformCoord(
		&ray->origin,
		&ray->origin,
		T) ;

	// transform the ray's direction, w = 0.
	D3DXVec3TransformNormal(
		&ray->direction,
		&ray->direction,
		T) ;

	// normalize the direction
	D3DXVec3Normalize(&ray->direction, &ray->direction) ;
}

D3DXVECTOR3 CPICKING::RaySquarTest(RAY* ray, D3DXPLANE* box)
{
	// 레이를 무한정 길게 뻗게 한다.
	D3DXVECTOR3 v, dir=ray->direction*10000 ;

	// 평면과 직선의 교차점을 구함.
	D3DXPlaneIntersectLine(
		&v,				// 평면과 직선의 교차점을 반환
		box,			// 평면
		&ray->origin,	// 라인의 시작점
		&dir) ;			// 라인의 끝점

	return v ;
}

VOID CPICKING::PickingTest()
{
	POINT mouse ;
	::GetCursorPos(&mouse) ;
	::ScreenToClient(g_hWnd, &mouse) ;

	if(GetAsyncKeyState(VK_LBUTTON) & 0x8001)
	{
		m_sRay = CalcPickingRay(mouse.x, mouse.y) ;

		// 뷰매트릭스 얻음
		D3DXMATRIX view ;
		m_pD3dDevice->GetTransform(D3DTS_VIEW, &view) ;

		// 뷰 매트릭스의 역행렬 -> 월드 공간
		D3DXMATRIX viewInverse ;
		D3DXMatrixInverse(&viewInverse, 0, &view) ;

		// 월드 공간에서의 광선 적용
		TransformRay(&m_sRay, &viewInverse) ;

		D3DXPLANE plane ;
		D3DXVECTOR3 point(0.0f, 0.0f, 0.0f) ;
		D3DXVECTOR3 normal(0.0f, 1.0f, 0.0f) ;
		D3DXPlaneFromPointNormal(&plane, &point, &normal) ;

		m_PickingPos = RaySquarTest(&m_sRay, &plane) ;
	}
}

POINT CPICKING::GetMousePoint()
{
	// 현재 마우스 좌표를 얻어와 m_pCurMouse에 저장(ScreenToClient를 이용해 로컬 좌표로 변환)
	GetCursorPos(&m_MousePos) ;
	ScreenToClient(g_hWnd, &m_MousePos) ;

	return m_MousePos ;
}

D3DXVECTOR3 CPICKING::GetPickingPoint()
{
	m_MousePos = this->GetMousePoint() ;

	/// 투영창의 포인트(x, y)점을 획득해 원점과 투영창의 포인트 점을 통과하는 광선을 구함.
	m_sRay = CalcPickingRay(m_MousePos.x, m_MousePos.y) ;

	/// 뷰매트릭스 얻음
	D3DXMATRIX view ;
	m_pD3dDevice->GetTransform(D3DTS_VIEW, &view) ;
	/// 뷰 매트릭스의 역행렬 -> 월드 공간
	D3DXMATRIX viewInverse ;
	D3DXMatrixInverse(&viewInverse, 0, &view) ;

	/// 월드 공간에서의 광선 적용
	TransformRay(&m_sRay, &viewInverse) ;

	return m_PickingPos ;
}

bool CPICKING::IntersectSphere(Sphere &sp, D3DXVECTOR3& inter, float& dist)
{
	this->GetPickingPoint() ;	// 픽킹 좌표와 광선값을 구함.

	D3DXVECTOR3 len = sp.vCenter - m_sRay.origin ;
	double s = D3DXVec3Dot(&len, &m_sRay.direction) ;
	double l2 = D3DXVec3Dot(&len, &len) ;
	double rad = pow(sp.fRad, 2) ;

	// 광선이 구의 반대 방향을 향하거나 구를 지나친 경우
	if(s<0 && l2>rad) return false ;

	double m2 = l2 - pow(s, 2) ;

	// 광선이 구를 빗겨가는 경우
	if(m2>rad) return false ;

	double q = sqrt(rad-m2) ;

	// 두 개의 교차점 중 어느 것을 구하는가?
	if(l2>rad)	dist = (float)(s - q) ;
	else		dist = (float)(s + q) ;

	inter = m_sRay.origin + m_sRay.direction * dist ;

	return true ;
}