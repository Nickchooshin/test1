#include "Light.h"

CLight::CLight(LPDIRECT3DDEVICE9 pD3dDevice)
: m_LightMode(LM_DIRECTION)
{
	m_pD3dDevice = pD3dDevice;
	
	InitMaterial();
	InitLight();
}

CLight::~CLight()
{

}

BOOL CLight::InitMaterial()
{
	ZeroMemory(&m_Material, sizeof(D3DMATERIAL9));

	// 확산광(Diffuse) 재질 설정 - 사실성을 위한 재질
	// (흰색)
	m_Material.Diffuse.r = 1.0f;
	m_Material.Diffuse.g = 1.0f;
	m_Material.Diffuse.b = 0.0f;
	m_Material.Diffuse.a = 1.0f;

	// 환경광(Ambient) 재질 설정 - 전체적인 색상의 출력을 위한 재질
	// (흰색)
	m_Material.Ambient.r = 1.0f;
	m_Material.Ambient.g = 1.0f;
	m_Material.Ambient.b = 0.0f;
	m_Material.Ambient.a = 1.0f;

	m_pD3dDevice->SetMaterial(&m_Material);

	return TRUE;
}

BOOL CLight::InitLight()
{
	ZeroMemory(&m_Light, sizeof(D3DLIGHT9));

	switch(m_LightMode)
	{
	case LM_POINT:					// 점광원
		m_Light.Type					= D3DLIGHT_POINT;									// 빛의 종류
		m_Light.Position				= D3DXVECTOR3(-5.0f, 5.0f, -5.0f);	// 빛의 위치
		m_Light.Range				= 10.0f;															// 빛의 도달 거리
		m_Light.Attenuation0	= 1.0f;															// 거리에 따라 감소하는 빛의 세기를 결정.
		m_Light.Attenuation1	= 1.0f;															// attenuation0 은 상수 감소, 1은 선형감소, 2는 2차 감소를 나타 낸다.
		m_Light.Attenuation1	= 0.0f;															//
		break;

	case LM_DIRECTION:			// 방향성 광원
		m_Light.Type					= D3DLIGHT_DIRECTIONAL;					// 빛의 종류
		m_Light.Diffuse.r			= 1.0f;
		m_Light.Diffuse.g			= 1.0f;
		m_Light.Diffuse.b			= 1.0f;
		m_LightDir						= D3DXVECTOR3(10.0f, -10.0f, -5.0f);// 빛의 방향
		D3DXVec3Normalize((D3DXVECTOR3*)&m_Light.Direction, &m_LightDir);	// 벡터 정규화
		break;

	case LM_SPOTLIGHT:		// 스포트라이트
		m_Light.Type					= D3DLIGHT_SPOT;									// 빛의 종류
		m_Light.Position				= D3DXVECTOR3(0.0f, 0.0f, 0.0f);		// 빛의 위치
		m_Light.Direction			= D3DXVECTOR3(1.0f, 0.0f, 0.0f);		// 빛의 방향
		m_Light.Range				= 1000.0f;													// 빛의 도달 거리
		m_Light.Attenuation0	= 1.0f;															// 거리에 따라 감소하는 빛의 세기를 결정.
		m_Light.Attenuation1	= 0.0f;															// attenuation0 은 상수 감소, 1은 선형감소, 2는 2차 감소를 나타 낸다.
		m_Light.Attenuation1	= 0.0f;															//
		m_Light.Falloff				= 1.0f;															// 안쪽원뿔과 바깥쪽 원뿔간의 빛의 세기
		m_Light.Theta					= 0.4f;															// 안쪽 원의 각도. (라디안 값)
		m_Light.Phi						= 0.9f;															// 바깥쪽 원의 각도. (라디안 값)
		break;
	}
	
	m_pD3dDevice->SetLight(0, &m_Light);
	m_pD3dDevice->LightEnable(0, TRUE);
	m_pD3dDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	m_pD3dDevice->SetRenderState( D3DRS_AMBIENT, 0x00808080 );

	return TRUE;
}

VOID CLight::UpdateLight()
{
	InitMaterial();
	InitLight();
}