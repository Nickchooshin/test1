#ifndef _BULLET_H_
#define _BULLET_H_

#include "Common.h"

struct BULLETVERTEX
{
	D3DXVECTOR3 position ;
	D3DCOLOR color ;
	float tu, tv ;
} ;

#define D3DFMT_BULLETVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)

class CBullet
{
private :
	LPDIRECT3DDEVICE9 m_pd3dDevice ;	// 렌더링에 사용될 D3D 디바이스
	LPDIRECT3DVERTEXBUFFER9 m_pVB ;	// 정점을 보관할 정점 버퍼
	LPDIRECT3DTEXTURE9 m_pTexture ;	// 텍스쳐

public :
	bool m_bState ;	// 현재 총알이 사용되고 있으면 true
	float m_fX, m_fY, m_fZ ;	// 현재 총알의 위치
	float m_fDeltaX, m_fDeltaZ ;	// 총알이 한 프레임 움직일 변위

public :
	CBullet(LPDIRECT3DDEVICE9 pd3dDevice) ;	// 오버로딩 생성자
	~CBullet() ;	// 소멸자

	HRESULT Initialize() ;
	HRESULT InitVB() ;
	HRESULT InitTexture() ;
	void Draw() ;
	void BulletControl() ;
	bool CrashCheck(float TankX, float TankZ) ;
} ;

#endif