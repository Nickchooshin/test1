#ifndef	_MESH_H_
#define	_MESH_H_

#include "Common.h"

class CMesh
{
public:
	// 매쉬 렌더링 모드
	enum MESH_RENDER_MODE
	{
		MRM_DEFAULT					= 1,		// 기본 모드
		MRM_SKYBOX					= 2,     // 스카이박스
		MRM_TANK							= 3,		// 탱크
	};

private:
	LPDIRECT3DDEVICE9			m_pd3dDevice;
	LPD3DXMESH							m_pMesh;							// 메쉬 객체
	D3DMATERIAL9*					m_pMeshMaterials;			// 매쉬에 대한 재질
	LPDIRECT3DTEXTURE9*		m_pMeshTextures;			// 매쉬에 대한 텍스쳐
	DWORD										m_dwNumMaterials;		// 재질의 수
	char*											xFileName;							// 매쉬 파일 이름

	MESH_RENDER_MODE		m_eMeshRenderMode;	// 메쉬 렌더 모드

public:
	CMesh( LPDIRECT3DDEVICE9 pd3dDevice, char* fileName, MESH_RENDER_MODE eMeshRenerMode );
	~CMesh( );

	HRESULT	InitGeometry( );
	VOID		Draw( );
};

#endif		// _MESH_H_