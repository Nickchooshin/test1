#ifndef _HEIGHT_MAP_H_
#define _HEIGHT_MAP_H

#include "Common.h"

struct HEIGHTMAPVERTEX
{
	D3DXVECTOR3 p ;
	D3DXVECTOR3 n ;
	D3DXVECTOR2 t ;
} ;

struct HEIGHTMAPINDEX
{
	WORD _0, _1, _2 ;
} ;

#define D3DFVF_HEIGHTMAPVERTEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

class CHeightmap
{
private :
	LPDIRECT3DDEVICE9 m_pd3dDevice ;
	LPDIRECT3DVERTEXBUFFER9 m_pVB ;
	LPDIRECT3DINDEXBUFFER9 m_pIB ;
	LPDIRECT3DTEXTURE9 m_pTexHeight ;
	LPDIRECT3DTEXTURE9 m_pTexDiffuse;

	DWORD m_cxHeight;
	DWORD m_czHeight;

	float m_fX, m_fY, m_fZ ;

public :
	CHeightmap(LPDIRECT3DDEVICE9 pd3dDevice) ;
	virtual ~CHeightmap() ;

	HRESULT InitGeometry() ;
	HRESULT InitTexture() ;
	HRESULT InitVB() ;
	HRESULT InitIB() ;
	void Draw() ;

	void SetupLights() ;
} ;

#endif