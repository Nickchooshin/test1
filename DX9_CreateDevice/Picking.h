#ifndef _PICKING_H_
#define _PICKING_H_

#include <d3d9.h>
#include <d3dx9.h>

#define SAFE_DELETE(x) if(x != NULL) { delete x; x = NULL; }
#define SAFE_DELETE_ARRAY(x) if(x != NULL) { delete [] x; x = NULL; }
#define SAFE_RELEASE(x) if(x != NULL) { x->Release(); x = NULL; }

// 픽킹 관련 // ---------------------------------------------------------------
struct PICKINGVERTEX
{
	D3DXVECTOR3 p;
	FLOAT tu, tv;
};

struct PICKINGINDEX
{
	WORD _0, _1, _2;
};

//#define D3DFVF_PICKINGVERTEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)
#define D3DFVF_PICKINGVERTEX (D3DFVF_XYZ | D3DFVF_TEX1)

extern HWND g_hWnd;

struct RAY
{
	D3DXVECTOR3 origin;
	D3DXVECTOR3 direction;
};

struct Sphere
{
	D3DXVECTOR3 vCenter ;
	float fRad ;
	Sphere(D3DXVECTOR3 vCen = D3DXVECTOR3(.0f, .0f, .0f), float _fRad=100.0f)
		: vCenter(vCen), fRad(_fRad)
	{}
} ;

class CPICKING
{
private:
	LPDIRECT3DDEVICE9		m_pD3dDevice;

	LPDIRECT3DVERTEXBUFFER9	m_pVB;				// 지형 정점
	LPDIRECT3DTEXTURE9		m_pTexture;			// 지형 텍스쳐

	D3DXMATRIX				m_Matrix;


	D3DXVECTOR3				m_PickingPos;
	POINT					m_MousePos;
	RAY						m_sRay;

public:
	CPICKING();
	CPICKING(LPDIRECT3DDEVICE9 pD3dDevice);
	~CPICKING();

	HRESULT		InitVB();
	HRESULT		InitTexture();
	VOID		Update();
	VOID		Draw();
	VOID		SetEulerMatrix();

	RAY			CalcPickingRay(int x, int y);
	VOID		TransformRay(RAY* ray, D3DXMATRIX* T);
	D3DXVECTOR3 RaySquarTest(RAY* ray, D3DXPLANE* box);

	POINT		GetMousePoint() ;
	D3DXVECTOR3	GetPickingPoint() ;
	bool		IntersectSphere(Sphere &sp, D3DXVECTOR3& inter, float& dist) ;

	VOID		PickingTest();
};

#endif // _PICKING_H_