#ifndef _FIRE_H_
#define _FIRE_H_

#include "Common.h"

struct FIREVERTEX
{
	D3DXVECTOR3 position ;
	D3DCOLOR color ;
	FLOAT tu, tv ;
} ;

#define D3DFVF_FIREVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)

class CFire
{
private :
	LPDIRECT3DDEVICE9 m_pd3dDevice ;
	LPDIRECT3DVERTEXBUFFER9 m_pVB ;
	LPDIRECT3DTEXTURE9 m_pTexture ;
public :
	// 폭발 스프라이트 처리를 위한 변수
	int m_iFireNumber ;			// 전체 스프라이트 이미지 수
	int m_iCurIndex ;			// 현재 출력해야 하는 스프라이트 인덱스
	int m_iFrameCounter	;		// 현재 스프라이트를 출력하고 지속된 프레임 수
	int m_iFrameDelay ;			// 스프라이트 변경 속도 조절을 위한 프레임 딜레이
	float m_fX, m_fY, m_fZ ;	// 스프라이트가 발생될 위치
	bool m_bState ;				// 폭발 처리 상태

public :
	CFire(LPDIRECT3DDEVICE9 pd3dDevice) ;
	virtual ~CFire() ;

	HRESULT InitGeometry() ;
	HRESULT InitTexture() ;
	HRESULT InitVB() ;
	HRESULT ChangeFireUV() ;
	void Draw() ;
} ;

#endif