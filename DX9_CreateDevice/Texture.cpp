#include "Texture.h"

CTexture::CTexture(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice = pd3dDevice;
	m_pVB = NULL;
	m_pIB = NULL;
	m_pTexture = NULL;
}

CTexture::~CTexture()
{
	SAFE_RELEASE(m_pd3dDevice);
	SAFE_RELEASE(m_pVB);
	SAFE_RELEASE(m_pIB);
	SAFE_RELEASE(m_pTexture);
}

HRESULT CTexture::Initailize()
{
	if(FAILED(InitVB()))			{ return E_FAIL; }
	if(FAILED(InitIB()))				{ return E_FAIL; }
	if(FAILED(InitTexture()))	{ return E_FAIL; }

	return S_OK;
}

HRESULT CTexture::InitVB()
{
	TEXTURECUBEVERTEX vertices[] = 
    {
        {-1.0f,  1.0f,  1.0f, 0xffffffff, 0.0f, 0.0f},   // v0
        { 1.0f,  1.0f,  1.0f, 0xffffffff, 1.0f, 0.0f},   // v1
        { 1.0f,  1.0f, -1.0f, 0xffffffff, 0.0f, 1.0f},   // v2
        {-1.0f,  1.0f, -1.0f, 0xffffffff, 1.0f, 1.0f},   // v3
        {-1.0f, -1.0f,  1.0f, 0xffffffff, 0.0f, 0.0f},   // v4
        { 1.0f, -1.0f,  1.0f, 0xffffffff, 1.0f, 0.0f},   // v5
        { 1.0f, -1.0f, -1.0f, 0xffffffff, 0.0f, 1.0f},   // v6
        {-1.0f, -1.0f, -1.0f, 0xffffffff, 1.0f, 1.0f},   // v7 
    };
 
    if(FAILED(m_pd3dDevice->CreateVertexBuffer(8*sizeof(TEXTURECUBEVERTEX), 0, D3DFMT_VIRTUALPLANEVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)))
    {
        return E_FAIL;
    }
 
    VOID* pVertices;
    if(FAILED(m_pVB->Lock(0, sizeof(vertices), (void**)&pVertices, 0)))
        return E_FAIL;
    memcpy(pVertices, vertices, sizeof(vertices));
    m_pVB->Unlock();
	
	return S_OK;
}

HRESULT CTexture::InitIB()
{
	TEXTURECUBEINDEX indices[] = 
    {
        {0, 1, 2}, {0, 2, 3}, // 윗면
        {4, 6, 5}, {4, 7, 6}, // 아랫면
        {0, 3, 7}, {0, 7, 4}, // 왼면
        {1, 5, 6}, {1, 6, 2}, // 오른면
        {3, 2, 6}, {3, 6, 7}, // 앞면
        {0, 4, 5}, {0, 5, 1}, // 뒷면
    };
 
    if(FAILED(m_pd3dDevice->CreateIndexBuffer(12*sizeof(TEXTURECUBEINDEX), 0, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_pIB, NULL)))
    {
        return E_FAIL;
    }
 
    VOID* pIndeces;
    if(FAILED(m_pIB->Lock(0, sizeof(indices), (void**)&pIndeces, 0)))
        return E_FAIL;
    memcpy(pIndeces, indices, sizeof(indices));
    m_pIB->Unlock();

	return S_OK;
}

HRESULT CTexture::InitTexture()
{
	if(FAILED(D3DXCreateTextureFromFile(m_pd3dDevice, "./Resource/env2.bmp", &m_pTexture))) {
		MessageBox(NULL, "이미지 파일 없음", "실패", MB_OK);
		return E_FAIL;
	}
	return S_OK;
}

VOID CTexture::Draw()
{
	D3DXMATRIXA16 matWorld, matTrans, matScale ;
	D3DXMatrixIdentity(&matWorld);
	D3DXMatrixTranslation(&matTrans, -2, 0, 2);
	D3DXMatrixScaling(&matScale, 10, 10, 10);
	matWorld = matWorld * matTrans * matScale ;
	m_pd3dDevice->SetTransform(D3DTS_WORLD, &matWorld);

	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(TEXTURECUBEVERTEX));		// 버텍스 데이터  
	m_pd3dDevice->SetFVF(D3DFMT_TEXTURECUBEVERTEX);												// 버텍스 정보 
	m_pd3dDevice->SetIndices(m_pIB);																							// 인덱스 정보	
	m_pd3dDevice->SetTexture(0, m_pTexture);																		// 텍스쳐 정보
	m_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);			// 렌더링
}