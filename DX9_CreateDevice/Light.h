#ifndef _LIHGT_H_
#define _LIGHT_H_

#include <d3d9.h>
#include <d3dx9.h>

class CLight
{
private:

	enum LIGHT_MODE
	{
		LM_POINT			= 1,
		LM_DIRECTION	= 2,
		LM_SPOTLIGHT	= 3,
		LM_DEFAULT		= LM_DIRECTION
	};

	LPDIRECT3DDEVICE9	m_pD3dDevice;
	LIGHT_MODE				m_LightMode;
	D3DMATERIAL9			m_Material;
	D3DLIGHT9					m_Light;
	D3DXVECTOR3			m_LightDir;

public:
	CLight(LPDIRECT3DDEVICE9 pD3dDevice);
	~CLight();

	BOOL	InitMaterial();
	BOOL	InitLight();
	VOID	UpdateLight();
};

#endif // _LIGHT_H_