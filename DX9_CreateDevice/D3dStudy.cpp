/* -----------------------------------------------------------------------------------------------------------------------------------
 * 2014-04-07 
 * 텍스쳐 맵핑을 따로 클래스를 만들어서 구현해 보자. 
 * copyright by sung-jun, Park (sjpark@hoseo.edu)  
 * ---------------------------------------------------------------------------------------------------------------------------------*/

#pragma region _전역변수_

#include "Common.h"
#include "Texture.h"
#include "Billboard.h"
#include "Light.h"
#include "Camera.h"
#include "Spaceship.h"
#include "Virtualplane.h"
#include "Terrain.h"
#include "Mesh.h"
#include "Bullet.h"
#include "Fire.h"
#include "Heightmap.h"
#include "trackball.h"
#include "Picking.h"

char szWinName[]	= "Direct3D";															// 윈도우 클래스 이름
char szWinTitle[]		= "Direct3D Framework";										// 윈도우 타이들 이름 

// --------------------------------------------------------------------------------------------------------------------------------------

HWND													g_hWnd;										// 윈도우 핸들

LPDIRECT3D9										g_pD3D			= NULL;					// D3D 디바이스를 생성할 D3D 객체 변수
LPDIRECT3DDEVICE9						g_pd3dDevice	= NULL;				// 렌더링에 사용될 D3D 디바이스

/// 객체 선언
CLight*													g_pLight;										// 조명객체 선언
CCamera*											g_pCamera;									// 카메라 객체 선언
CTexture*												g_pTexture;									// 텍스쳐 맵핑 객체 선언
CBillboard*											g_pBillboard;								// 빌보드 처리를 위한 객체 선언
CSpaceship*										g_pSpaceship;								// 우주선 객체 선언
CVirtualplane*									g_pVirtualplane;							// 가상평면 객체 선언
CTerrain*												g_pTerrain;									// 지형평면 객체 선언
CMesh*													g_pSkybox;									// 스카이박스 객체 선언
CMesh*													g_pTank;										// 탱크 객체 선언
CBullet*										g_pBullet ;					// 총알 객체 선언
CFire*											g_pFire ;
CMesh*											g_pTank2[2] ;
CHeightmap*										g_pHeightmap ;
CPICKING*			g_pPicking;				// 픽킹

trackball_t										g_pTrackball ;
D3DXMATRIXA16									g_matWorld ;
bool											g_bZoomDrag ;
float											g_fZoom = -5.0f;

/// 구조체 배열 선언
SPACESHIPOBJECT								g_pObjects[2];							// 우주선 각각의 로컬 위치 정보를 갖고 있는 매트릭스

D3DXMATRIX										g_matTank;

#pragma endregion

/* -----------------------------------------------------------------------------------------------------------------------------------
 * InitD3D( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
HRESULT InitD3D()
{
	// 디바이스를 생성하기 위한 D3D 객체 생성
    if(NULL == (g_pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
        return E_FAIL; 

	// 표면 (Surface) 설정
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory(&d3dpp, sizeof(d3dpp));
    d3dpp.Windowed								= TRUE;
    d3dpp.SwapEffect								= D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat					= D3DFMT_UNKNOWN; 
	d3dpp.EnableAutoDepthStencil	= TRUE;
	d3dpp.AutoDepthStencilFormat	= D3DFMT_D16;

	// 디바이스 생성
    if(FAILED(g_pD3D->CreateDevice(
									D3DADAPTER_DEFAULT, 
                                    D3DDEVTYPE_HAL, 
                                    g_hWnd, 
                                    D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
                                    &d3dpp, 
                                    &g_pd3dDevice)))
        return E_FAIL; 

    return S_OK;
} 

/* -----------------------------------------------------------------------------------------------------------------------------------
 * InitGeometry( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
HRESULT InitGeometry()
{
	// 우주선의 로컬 좌표 정의(초기화)
    D3DXMatrixIdentity(&g_pObjects[0].matLocal);
    g_pObjects[0].matLocal._41 = 0.0;
    g_pObjects[0].matLocal._42 = 0.2f; 
    g_pObjects[0].matLocal._43 = -1.0;  

	// 객체 초기화 // ----------------------------------------------
	// (1) Light
	g_pLight = new CLight(g_pd3dDevice);

	// (2) Texture Mapping
	g_pTexture = new CTexture(g_pd3dDevice);
	g_pTexture->Initailize();
	
	// (3) Billboarding
	g_pBillboard = new CBillboard(g_pd3dDevice);
	g_pBillboard->Initailize();

	// (4) Spaceship
	g_pSpaceship = new CSpaceship(g_pd3dDevice);
	g_pSpaceship->Initailize();

	// (5) Virtualplane
	g_pVirtualplane = new CVirtualplane(g_pd3dDevice);
	g_pVirtualplane->Initailize();

	// (6) Camera
	g_pCamera = new CCamera(g_pd3dDevice);

	// (7) Terrain
	g_pTerrain = new CTerrain(g_pd3dDevice);
	g_pTerrain->Initailize();

	// (8) SkyBox
	g_pSkybox = new CMesh(g_pd3dDevice, "./Resource/skybox2.x", CMesh::MRM_SKYBOX);

	// (9) Tank
	g_pTank = new CMesh(g_pd3dDevice, "./Resource/predator.x", CMesh::MRM_TANK);

	// (10) Bullet
	g_pBullet = new CBullet(g_pd3dDevice) ;

	g_pFire = new CFire(g_pd3dDevice) ;

	g_pHeightmap = new CHeightmap(g_pd3dDevice) ;

	g_pTrackball.Init(400, 400) ;
	D3DXMatrixIdentity(&g_matWorld);

	g_pPicking = new CPICKING(g_pd3dDevice) ;

	for(int i=0; i<2; i++)
		g_pTank2[i] = new CMesh(g_pd3dDevice, "./Resource/predator.x", CMesh::MRM_TANK) ;

	return S_OK;
}

/* -----------------------------------------------------------------------------------------------------------------------------------
 * SetupRenderStates( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
VOID SetupRenderStates()
{
	// 컬링 모드 셋팅 : 뒤의 면 출력
	g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	// 조명 셋팅 : 조명 없음
	//g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	// 폴리곤 출력 형태 : 와이어 프레임 형태
	//g_pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID /* D3DFILL_WIREFAME*/);
}

/* -----------------------------------------------------------------------------------------------------------------------------------
 * SetupInputs( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
VOID SetupInputs()
{
	g_pTrackball.TrackMouse(g_hWnd, g_bZoomDrag=true, &g_fZoom);  // track mouse movements and move object
	g_pTrackball.SetMatrix(g_matWorld);  // get the world matrix for the trackball

    FLOAT fOneStep = 0.1f;

    if(GetAsyncKeyState('W')) 		//앞으로전진
    {
         g_pObjects[0].matLocal._43 += fOneStep;
    }
 
    if(GetAsyncKeyState('S')) 			//뒤로후진
    {
        g_pObjects[0].matLocal._43 -= fOneStep;
    }
 
    if(GetAsyncKeyState('D')) 			//우로이동
    {
        g_pObjects[0].matLocal._41 += fOneStep;
    }
 
    if(GetAsyncKeyState('A')) 			//좌로이동
    {
        g_pObjects[0].matLocal._41 -= fOneStep;
    }

	// 전진
	if(GetAsyncKeyState(VK_UP))		
	{ 
		g_pCamera->m_sPolar.x += (float) ( g_pCamera->m_sPolar.radius * cos ( g_pCamera->m_sPolar.angle * ( D3DX_PI / 180.0f ) ) ); 
		g_pCamera->m_sPolar.z += (float) ( g_pCamera->m_sPolar.radius * sin ( g_pCamera->m_sPolar.angle * ( D3DX_PI / 180.0f ) ) );
	}
	// 후진
	if(GetAsyncKeyState(VK_DOWN))	
	{ 
		g_pCamera->m_sPolar.x -= (float) ( g_pCamera->m_sPolar.radius * cos ( g_pCamera->m_sPolar.angle * ( D3DX_PI / 180.0f ) ) );
		g_pCamera->m_sPolar.z -= (float) ( g_pCamera->m_sPolar.radius * sin ( g_pCamera->m_sPolar.angle * ( D3DX_PI / 180.0f ) ) );
	}
	// 좌로 회전
	if(GetAsyncKeyState(VK_LEFT))		
	{ 
		g_pCamera->m_sPolar.angle += 1;
	}
	// 우로 회전
	if(GetAsyncKeyState(VK_RIGHT))	
	{ 
		g_pCamera->m_sPolar.angle -= 1;
	}

	// 총알
	if(GetAsyncKeyState(VK_SPACE))
	{
		if(g_pBullet->m_bState==false)
		{
			g_pBullet->m_bState = true ;

			// 총알의 최초 위치는 카메라의 위치로 설정
			g_pBullet->m_fX = g_pCamera->m_sPolar.x ;
			g_pBullet->m_fY = g_pCamera->m_sPolar.y ;
			g_pBullet->m_fZ = g_pCamera->m_sPolar.z ;

			// 매 프레임 마다 총알이 이동할 범위를 설정
			float speed = 10.0f ;
			g_pBullet->m_fDeltaX = (float) (speed * cos(g_pCamera->m_sPolar.angle * (D3DX_PI / 180.0f))) ;
			g_pBullet->m_fDeltaZ = (float) (speed * sin(g_pCamera->m_sPolar.angle * (D3DX_PI / 180.0f))) ;
		}
	}

	// ESC
	if(GetAsyncKeyState(VK_ESCAPE))	{ PostQuitMessage(0);  }		
}

/* -----------------------------------------------------------------------------------------------------------------------------------
 * Render( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
VOID Render()
{
#pragma region _오일러 회전_
	/* 
	FLOAT fOneStep = 0.1;
	FLOAT fXAngle = 0.0f;

	D3DXVECTOR3 vRight;	// x 축 벡터 (회전 : Pitch)
	D3DXVECTOR3 vUp;		// y 축 벡터 (회전 : Yaw) 
	D3DXVECTOR3 vLook;	// z 축 벡터 (회전 : Roll)
	D3DXVECTOR3 vPos;		// 현재 위치(x, y, z)

	// (1) 우주선의 로컬 정보를 가져옴 
	vRight.x	= g_pObjects[0].matLocal._11; 
	vRight.y	= g_pObjects[0].matLocal._12; 
	vRight.z	= g_pObjects[0].matLocal._13;
	vUp.x		= g_pObjects[0].matLocal._21;
	vUp.y		= g_pObjects[0].matLocal._22;
	vUp.z		= g_pObjects[0].matLocal._23;
	vLook.x	= g_pObjects[0].matLocal._31;
	vLook.y	= g_pObjects[0].matLocal._32;
	vLook.z	= g_pObjects[0].matLocal._33;
	vPos.x		= g_pObjects[0].matLocal._41;
	vPos.y		= g_pObjects[0].matLocal._42;
	vPos.z		= g_pObjects[0].matLocal._43; 

	// (2) 가져온 로컬 축(벡터)을 정규화
	D3DXVec3Normalize(&vLook, &vLook);
	D3DXVec3Cross(&vRight, &vUp, &vLook);  // UP 벡터, LOOK 벡터 의 외적 구하기
	D3DXVec3Normalize(&vRight, &vRight);
	D3DXVec3Cross(&vUp, &vLook, &vRight); // RIGHT 벡터, LOOK 벡터의 외적 구하기
	D3DXVec3Normalize(&vUp, &vUp); 

	// (3) 회전 행렬을 구함 - pitch, yaw and roll 행렬

	/// 회전 각도
	FLOAT  fPitch	= 0.0f; 
	FLOAT  fYaw	= 0.0f;
	FLOAT  fRoll	= 0.0f;
	FLOAT  fspeed = fOneStep;

	if(GetAsyncKeyState('D'))		fRoll -= fOneStep;
	if(GetAsyncKeyState('A'))		fRoll += fOneStep;
	if(GetAsyncKeyState('S'))		fPitch -= fOneStep;
	if(GetAsyncKeyState('W'))		fPitch += fOneStep;
	if(GetAsyncKeyState('Q'))		fYaw -= fOneStep;
	if(GetAsyncKeyState('E'))		fYaw += fOneStep;

	D3DXMATRIX matPitch, matYaw, matRoll;
	D3DXMatrixRotationAxis(&matPitch, &vRight, fPitch );	// x 축 회전 행렬
	D3DXMatrixRotationAxis(&matYaw, &vUp, fYaw );        // y 축 회전 행렬
	D3DXMatrixRotationAxis(&matRoll, &vLook, fRoll);        // z 축 회전 행렬

	// (4) 회전 벡터를 구함
	// UP Vector 를 중심으로 Look vector, Right vector 회전
	// Y축을 회전 하기 위해서는 z축 벡터, x축 벡터를 회전 시켜야 함 
	D3DXVec3TransformCoord(&vLook, &vLook, &matYaw); 
	D3DXVec3TransformCoord(&vRight, &vRight, &matYaw);
	// Right Vector 를 중심으로 Look vector, Up vector 회전
	// x축을 회전 하기 위해서는 y축 벡터, z축 벡터를 회전 시켜야 함 
	D3DXVec3TransformCoord(&vLook, &vLook, &matPitch); 
	D3DXVec3TransformCoord(&vUp, &vUp, &matPitch);
	// Look Vector 를 중심으로 Right vector, Up vector 회전
	// z축을 회전 하기 위해서는 x축 벡터, y축 벡터를 회전 시켜야 함 
	D3DXVec3TransformCoord(&vRight, &vRight, &matRoll); 
	D3DXVec3TransformCoord(&vUp, &vUp, &matRoll);

	// (5) 회전 벡터와 이동 벡터를 우주선의 로컬 행렬에 다시 셋팅
	g_pObjects[0].matLocal._11 = vRight.x; 
	g_pObjects[0].matLocal._12 = vRight.y; 
	g_pObjects[0].matLocal._13 = vRight.z;
	g_pObjects[0].matLocal._21 = vUp.x; 
	g_pObjects[0].matLocal._22 = vUp.y; 
	g_pObjects[0].matLocal._23 = vUp.z;
	g_pObjects[0].matLocal._31 = vLook.x; 
	g_pObjects[0].matLocal._32 = vLook.y; 
	g_pObjects[0].matLocal._33 = vLook.z;
	//g_pObjects[0].matLocal._41 = vPos.x;
	//g_pObjects[0].matLocal._42 = vPos.y;
	//g_pObjects[0].matLocal._43 = vPos.z; 
	*/
#pragma endregion

#pragma region _쿼터니온 회전_
	/*
	FLOAT fOneStep = 0.1f;
	
	FLOAT  fPitch	= 0.0f;			// x 축 회전 각도
	FLOAT  fYaw	= 0.0f;			// y 축 회전 각도
	FLOAT  fRoll	= 0.0f;				// z 축 회전 각도

	if(GetAsyncKeyState('D'))		fRoll -= fOneStep;
	if(GetAsyncKeyState('A'))		fRoll += fOneStep;
	if(GetAsyncKeyState('S'))			fPitch -= fOneStep;
	if(GetAsyncKeyState('W'))		fPitch += fOneStep;
	if(GetAsyncKeyState('Q'))		fYaw -= fOneStep;
	if(GetAsyncKeyState('E'))			fYaw += fOneStep;
	
	D3DXQUATERNION qR;		// 쿼터니온
	D3DXMATRIX   matRot;		// 회전 행렬
	
	D3DXQuaternionRotationYawPitchRoll(&qR, fYaw, fPitch, fRoll);			// 4원수 구하기
	D3DXMatrixRotationQuaternion(&matRot, &qR);									    // 4원수를 가지고 회전 행렬 만들기
	D3DXMatrixMultiply(&g_pObjects[0].matLocal, &matRot, &g_pObjects[0].matLocal);	// 우주선 로컬 정보에 셋팅
	*/
#pragma endregion

    if(NULL == g_pd3dDevice)
        return; 

    g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DXCOLOR(0.0f,0.25f,0.25f,0.55f), 1.0f, 0); 

	g_pLight->UpdateLight();
	g_pCamera->UpdateCamera();
	
	SetupRenderStates();
	SetupInputs();

    if(SUCCEEDED(g_pd3dDevice->BeginScene()))	/// (렌더링 시작)-------------------------------------------------
    {
		D3DXMATRIXA16 matTrans, matScale, matView, matViewInv, matWorld;
		/*D3DXMatrixIdentity(&matWorld);

		/// (1) 가상평면 그리기
		//g_pVirtualplane->Draw();	        
		g_pTerrain->Draw();

		/// (2) 텍스쳐 입힌 큐브 그리기  
		//D3DXMatrixTranslation(&matTrans, -2, 0, 2);
		//D3DXMatrixScaling(&matScale, 10, 10, 10);
		//g_pd3dDevice->SetTransform(D3DTS_WORLD, &(matTrans*matScale) );
		g_pTexture->Draw();		

		/// (3) 우주선 그리기
		D3DXMatrixScaling(&matScale, 20, 20, 20);
		g_pd3dDevice->SetTransform(D3DTS_WORLD, &(g_pObjects[0].matLocal*matScale));
		g_pSpaceship->Draw();
		 		  
		/// (4) SkyBox 그리기
		g_pSkybox->Draw();

		/// (5) 빌보드용 나무 그리기	
		//D3DXMatrixTranslation(&matTrans, 2, 1, 5);
		//D3DXMatrixScaling(&matScale, 20, 20, 20);
		//g_pd3dDevice->SetTransform(D3DTS_WORLD, &(matTrans*matScale));
		g_pBillboard->Draw();

		/// (6) 탱크 그리기
		matView = g_pCamera->GetViewMatrix();										// 카메라의 역행렬을 구해서 탱크의 좌표 월드행렬에 곱해줌
		D3DXMatrixInverse(&matViewInv, NULL, &matView);					//
		D3DXMatrixTranslation(&matTrans, -1.0f, -8.0f, 20.0f);				// 이동 적용
		D3DXMatrixMultiply(&matWorld, &matTrans, &matViewInv);
		D3DXMatrixScaling(&matScale, 0.02f, 0.02f, 0.02f);						// 스케일 적용
		D3DXMatrixMultiply(&matWorld, &matScale, &matWorld);
		g_pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld);
		g_pTank->Draw();

		for(int i=0; i<2; i++)
		{
			D3DXMatrixIdentity(&matWorld);
			//D3DXMatrixTranslation(&matTrans, -5.0f+(-5.0f * i), -8.0f, 8.0f) ;
			D3DXMatrixTranslation(&matTrans, -1.0f, -8.0f, 20.0f) ;
			//D3DXMatrixMultiply(&matWorld, &matTrans, &matScale) ;
			g_pd3dDevice->SetTransform(D3DTS_WORLD, &(matWorld * matTrans * matScale)) ;
			g_pTank2[i]->Draw() ;
		}

		// 총알
		if(g_pBullet->m_bState==true)
		{
			D3DXMATRIXA16 matBillboard = g_pCamera->GetBillBoardMatrix() ;
			matBillboard._41 = g_pBullet->m_fX + 1.35f ;
			matBillboard._42 = 0 ;
			matBillboard._43 = g_pBullet->m_fZ ;
			g_pd3dDevice->SetTransform(D3DTS_WORLD, &matBillboard) ;

			if(g_pBullet->CrashCheck(g_pObjects[0].matLocal._41 * 20, g_pObjects[0].matLocal._43 * 20))
			{
				g_pFire->m_fX = g_pBullet->m_fX ;
				g_pFire->m_fZ = g_pBullet->m_fZ ;
				g_pFire->m_iCurIndex = 0 ;
				g_pFire->m_bState = true ;
			}
			g_pBullet->Draw() ;
		}

		if(g_pFire->m_bState==true)
		{
			D3DXMATRIXA16 matBillboard = g_pCamera->GetBillBoardMatrix() ;
			matBillboard._41 = g_pFire->m_fX + 1.35f ;
			matBillboard._42 = 0 ;
			matBillboard._43 = g_pFire->m_fZ ;
			g_pd3dDevice->SetTransform(D3DTS_WORLD, &matBillboard) ;

			g_pFire->Draw() ;
		}

		D3DXMatrixIdentity(&matWorld) ;
		g_pd3dDevice->SetTransform(D3DTS_WORLD, &matWorld) ;
		g_pHeightmap->Draw() ;*/

		//g_pPicking->PickingTest() ;
		//g_pPicking->Draw() ;
		g_pPicking->Update() ;

		g_pd3dDevice->EndScene(); /// (렌더링 끝) -------------------------------------------------------------------------
    } 
    g_pd3dDevice->Present(NULL, NULL, NULL, NULL); 
} 

/* -----------------------------------------------------------------------------------------------------------------------------------
 * Cleanup( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
VOID Cleanup()
{
	if(g_pCamera != NULL)				SAFE_DELETE(g_pCamera);			// 카메라 객체 해제
	if(g_pLight != NULL)					SAFE_DELETE(g_pLight);					// 조명 객체 해제
	if(g_pTexture != NULL)				SAFE_DELETE(g_pTexture);				// 텍스쳐맵핑 객체 해제
	if(g_pBillboard != NULL)			SAFE_DELETE(g_pBillboard);			// 빌보드 객체 해제
	if(g_pSpaceship != NULL)		SAFE_DELETE(g_pSpaceship);		// 우주선 객체 해제
	if(g_pVirtualplane != NULL)	SAFE_DELETE(g_pVirtualplane);	// 가상평면 객체 해제
	if(g_pTerrain != NULL)				SAFE_DELETE(g_pVirtualplane);	// 평면 지형 객체 해제

	if(g_pSkybox != NULL)	SAFE_DELETE(g_pSkybox) ;
	if(g_pTank != NULL)		SAFE_DELETE(g_pTank) ;
	if(g_pBullet != NULL)	SAFE_DELETE(g_pBullet) ;

	if(g_pFire != NULL)		SAFE_DELETE(g_pFire) ;
	for(int i=0; i<2; i++)
	{
		if(g_pTank2[i]!=NULL) SAFE_DELETE(g_pTank2[i]) ;
	}

	if(g_pHeightmap != NULL)	SAFE_DELETE(g_pHeightmap) ;

    if(g_pd3dDevice != NULL)		g_pd3dDevice->Release();				// DirectX9 디바이스 객체 
    if(g_pD3D != NULL)					g_pD3D->Release();							// DirectX9 객체	
} 

/* -----------------------------------------------------------------------------------------------------------------------------------
 * WndProc( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message) {
        case WM_DESTROY :    // 프로그램 종료
			Cleanup();
			PostQuitMessage(WM_QUIT);
			break;
    } 
    return DefWindowProc(hwnd, message, wParam, lParam);
}  

/* -----------------------------------------------------------------------------------------------------------------------------------
 * WinMain( ) 함수
 * ---------------------------------------------------------------------------------------------------------------------------------*/
INT WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPSTR lpszArgs, INT nWinMode)
{
    /* STEP 1 : 윈도우 클래스 정의--------------------------------------------------------------------*/ 
    WNDCLASS wcl;
    wcl.hInstance				= hThisInst;					// 인스턴스에 대한 핸들 
    wcl.lpszClassName		= szWinName;				// 윈도우 클래스 이름 
    wcl.lpfnWndProc		= WndProc;					// 윈도우 콜백 함수 
    wcl.style						= 0;									// 기본 스타일 
    wcl.hIcon						= NULL;							// 표준 아이콘(스타일) 사용 
    wcl.hCursor					= NULL;							// 표준 커서(스타일) 사용 
    wcl.lpszMenuName	= NULL;							// 메뉴 사용하지 않음 
    wcl.cbClsExtra				= 0;									// 추가 정보는 사용하지 않음 
    wcl.cbWndExtra			= 0;									// 추가 정보는 사용하지 않음 
    wcl.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);	// 윈도우 배경을 하얀색으로 함
         
    /* STEP 2 : 윈도우 클래스 등록--------------------------------------------------------------------*/ 
    if(!RegisterClass(&wcl))
        return 0; 

    /* STEP 3 : 윈도우 생성 --------------------------------------------------------------------------*/ 
	g_hWnd = CreateWindow(
					szWinName,									// 윈도우 클래스 이름
                    szWinTitle,									// 윈도우 타이틀
                    WS_SYSMENU,							// 윈도우 스타일 - normal
                    CW_USEDEFAULT,						// X 좌표 윈도우가 결정
                    CW_USEDEFAULT,						// Y 좌표 윈도우가 결정
                    1200,												// 윈도우 넓이
                    1200,												// 윈도우 높이
                    HWND_DESKTOP,						// 부모 윈도우 없음
                    NULL,												// 메뉴 없음
                    hThisInst,										// 이 프로그램에 대한 인스턴스 핸들
                    NULL												// 추가적인 매개 변수 없음  
				); 

	if(SUCCEEDED(InitD3D()))
    {
		if(SUCCEEDED(InitGeometry()))
        {
			/* STEP 4 : 윈도우 시현 ---------------------------------------------------------------------*/ 
            ShowWindow(g_hWnd, SW_SHOWDEFAULT);
            UpdateWindow(g_hWnd);
   
            /* STEP 5 : 메세지 루프 생성 -----------------------------------------------------------------*/ 
            MSG msg;
            ZeroMemory(&msg, sizeof(msg)); 

            while(WM_QUIT != msg.message)
            {
				if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                {
					TranslateMessage(&msg);		// 키보드 입력 허용
                    DispatchMessage(&msg);		// 윈도우로 제어권 반환
                }
                else
				{
					Render(); //처리할 메세지가 없으면 Render 함수 호출   
				}
            }
		}
	}
    UnregisterClass(szWinName, wcl.hInstance);

    return 0;
}

