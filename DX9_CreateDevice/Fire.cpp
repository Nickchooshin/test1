#include "Fire.h"

CFire::CFire(LPDIRECT3DDEVICE9 pd3dDevice) : m_pVB(NULL),
											 m_pTexture(NULL)
{
	m_pd3dDevice = pd3dDevice ;

	m_iFireNumber = 15 ;
	m_iCurIndex = 0 ;
	m_iFrameCounter = 0 ;
	m_iFrameDelay = 1 ;
	m_fX = 0 ;
	m_fY = 0 ;
	m_fZ = 0 ;
	m_bState = false ;

	InitGeometry() ;
}
CFire::~CFire()
{
}

HRESULT CFire::InitGeometry()
{
	InitTexture() ;
	InitVB() ;

	return S_OK ;
}

HRESULT CFire::InitTexture()
{
	if( FAILED(D3DXCreateTextureFromFile(m_pd3dDevice, "./Resource/Fire.bmp", &m_pTexture)) )
	{
		MessageBox(NULL, "BMP파일 로드 실패", "텍스쳐 로드 실패", MB_OK) ;
		return E_FAIL ;
	}

	return S_OK ;
}

HRESULT CFire::InitVB()
{
	if( FAILED(m_pd3dDevice->CreateVertexBuffer(4 * sizeof(FIREVERTEX), 0, D3DFVF_FIREVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)) )
	{
		return E_FAIL ;
	}

	// 버텍스 버퍼 설정
	FIREVERTEX *pVertices ;
	if( FAILED(m_pVB->Lock(0, 0, (void**)&pVertices, 0)) )
		return E_FAIL ;

	pVertices[0].position = D3DXVECTOR3(-30, 50, 100) ;	// 버텍스 위치
	pVertices[0].color = 0xffffffff ;					// 버텍스 알파 및 색상
	pVertices[0].tu = 0.0f ;							// 버텍스 U 텍스쳐 좌표
	pVertices[0].tv = 0.0f ;							// 버텍스 V 텍스쳐 좌표

	pVertices[1].position = D3DXVECTOR3(30, 50, 100) ;
	pVertices[1].color = 0xffffffff ;
	pVertices[1].tu = 64.0f/960.0f ;
	pVertices[1].tv = 0.0f ;

	pVertices[2].position = D3DXVECTOR3(-30, -10, 100) ;
	pVertices[2].color = 0xffffffff ;
	pVertices[2].tu = 0.0f ;
	pVertices[2].tv = 1.0f ;

	pVertices[3].position = D3DXVECTOR3(30, -10, 100) ;
	pVertices[3].color = 0xffffffff ;
	pVertices[3].tu = 64.0f/960.0f ;
	pVertices[3].tv = 1.0f ;

	m_pVB->Unlock() ;

	return S_OK ;
}

void CFire::Draw()
{
	m_pd3dDevice->SetRenderState(D3DRS_LIGHTING, FALSE) ;

	// 텍스쳐 설정 (텍스쳐 매핑을 위하여 g_pTexture를 사용하였다.)
	m_pd3dDevice->SetTexture(0, m_pTexture) ;

	// 텍스쳐 출력 환경 설정
	//m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE) ;
	//m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE) ;
	//m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE) ;
	//m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE) ;

	// 버텍스들의 알파셋에 대하여 블렌딩 설정
	m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true) ;
	m_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE) ;
	m_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE) ;

	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(FIREVERTEX)) ; // 출력할 버텍스 버퍼 설정
	m_pd3dDevice->SetFVF(D3DFVF_FIREVERTEX) ;	// FVF 값 설정
	m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2) ; //사각형 영역 (삼각형 2개를 이용하여 사각형 영역을 만들었음) 출력

	// 텍스쳐 설정 해제
	m_pd3dDevice->SetTexture(0, NULL) ;

	// 버텍스들의 알파셋에 대하여 블렌딩 설정
	m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false) ;

	// 폭발 스프라이트 애니메이션을 위한 uv 애니메이션 함수 호출
	ChangeFireUV() ;
}

HRESULT CFire::ChangeFireUV()
{
	float u = (m_iCurIndex * 64.0f) / 960.0f ;	// 현재 인덱스를 이용한 u 계산
	float u2 = ((m_iCurIndex+1) * 64.0f) / 960.0f ;	// 현재 인덱스+1을 이용한 u 계산

	FIREVERTEX *pVertices ; // 버텍스 버퍼 접근용 포인터

	if( FAILED(m_pVB->Lock(0, 0, (void**)&pVertices, 0)) )
		return E_FAIL ;

	pVertices[0].tu = u ;	// u 좌표 변경
	pVertices[0].tv = 0.0f ;

	pVertices[1].tu = u2 ;
	pVertices[1].tv = 0.0f ;

	pVertices[2].tu = u ;
	pVertices[2].tv = 1.0f ;

	pVertices[3].tu = u2 ;
	pVertices[3].tv = 1.0f ;

	m_pVB->Unlock() ;

	// 지정된 딜레이 프레임이 지난 경우
	if(m_iFrameCounter>=m_iFireNumber)
	{
		m_iCurIndex = (m_iCurIndex+1) % m_iFireNumber ; // 인덱스 변경
		m_iFrameCounter = 0 ; // 프레임 카운터 초기화

		if(m_iCurIndex==0)
			m_bState = false ;
	}
	else // 아직 변경할 시간이 안된 경우
		m_iFrameCounter++ ; // 프레임 카운터 증가

	return S_OK ;
}