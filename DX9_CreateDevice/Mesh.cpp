#include "Mesh.h"

CMesh::CMesh( LPDIRECT3DDEVICE9 pd3dDevice,  char*	fileName, MESH_RENDER_MODE eMeshRenderMode)
{
	m_pd3dDevice = pd3dDevice;
	xFileName = fileName;
	m_eMeshRenderMode = eMeshRenderMode;
	InitGeometry( );
}

CMesh::~CMesh( )
{
}

HRESULT CMesh::InitGeometry( )
{
	// 재질을 임시로 보관할 버퍼 선언
	LPD3DXBUFFER pD3DXMtrlBuffer;

	// x 파일을 읽어 들인다. - 이 때 재질 정보도 함께 읽어 들인다. 
	if( FAILED ( D3DXLoadMeshFromX( 
		xFileName, 
		D3DXMESH_SYSTEMMEM, 
		m_pd3dDevice, 
		NULL, 
		&pD3DXMtrlBuffer, 
		NULL, 
		&m_dwNumMaterials, 
		&m_pMesh
		) ) )
	{
		MessageBox(NULL, "X파일 로드 실패", "메쉬 로드 실패", MB_OK);
		return E_FAIL;
	}


	char texturePath[256];
	if(strchr(xFileName, '/') == NULL)
		wsprintf(texturePath, "./");
	else {
		char temp[256], *pChar;
		strcpy(temp, xFileName);
		_strrev(temp);
		pChar = strchr(temp, '/');
		strcpy(texturePath, pChar);
		_strrev(texturePath);
	}


	// 재질 정보와 텍스쳐 정보를 따로 뽑아 낸다. 
	D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL* ) pD3DXMtrlBuffer->GetBufferPointer( );

	m_pMeshMaterials = new D3DMATERIAL9[ m_dwNumMaterials ];			// 재질 개수 만큼 재질 구조체 배열 생성
	m_pMeshTextures = new LPDIRECT3DTEXTURE9[ m_dwNumMaterials ];	// 재질 개수 만큼 텍스쳐 구조체 배열 생성

	for( DWORD i=0; i<m_dwNumMaterials; i++)
	{
		// 재질 정보 복사
		m_pMeshMaterials[i] = d3dxMaterials[i].MatD3D;
		// 주변 광원 정보를 Diffuse 정보로
		m_pMeshMaterials[i].Ambient = m_pMeshMaterials[i].Diffuse;

		m_pMeshTextures[i] = NULL;

		if( d3dxMaterials[i].pTextureFilename != NULL && lstrlen(d3dxMaterials[i].pTextureFilename ) > 0 )
		{
			char tempFile[256];
			wsprintf(tempFile, "%s%s", texturePath, d3dxMaterials[i].pTextureFilename);

			if( FAILED (D3DXCreateTextureFromFile (m_pd3dDevice, tempFile, &m_pMeshTextures[i] ) ) )
			{
				m_pMeshTextures[i] = NULL;
			}
		}
	}

	// 임시로 생성한 재질 버퍼 소거
	pD3DXMtrlBuffer->Release( );

	return S_OK;
}

VOID CMesh::Draw( )
{
	D3DXMATRIXA16 matWorld, matWorld2;

	switch(m_eMeshRenderMode)
	{
		case MRM_DEFAULT :
			break;

		case MRM_SKYBOX:
			D3DXMatrixIdentity(&matWorld);
			D3DXMatrixScaling(&matWorld, 60.0f, 60.0f, 60.0f);
			m_pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld);
			break;

		case MRM_TANK:
			//D3DXMatrixIdentity(&matWorld);
			//D3DXMatrixIdentity(&matWorld2);
			//D3DXMatrixTranslation( &matWorld, 20, 0, 0 );
			//D3DXMatrixScaling(&matWorld2, 0.05f, 0.05f, 0.05f);
			//D3DXMatrixMultiply(&matWorld, &matWorld2, &matWorld);
			//D3DXMatrixRotationY( &matWorld2, (1.0f*(D3DX_PI / 180.0f)));
			//D3DXMatrixMultiply(&matWorld, &matWorld2, &matWorld);
			//m_pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld);
			break;
	}

	// 텍스쳐 설정
	m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,		D3DTOP_MODULATE);
	m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1,	D3DTA_TEXTURE);
	m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2,	D3DTA_DIFFUSE);

	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER,	D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER,	D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MIPFILTER,	D3DTEXF_LINEAR);

	for( DWORD i=0; i<m_dwNumMaterials; i++)
	{
		m_pd3dDevice->SetMaterial(&m_pMeshMaterials[i]);
		m_pd3dDevice->SetTexture(0, m_pMeshTextures[i]);
		m_pMesh->DrawSubset(i);
	}
}