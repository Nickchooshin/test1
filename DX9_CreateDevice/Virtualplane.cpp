#include "Virtualplane.h"

CVirtualplane::CVirtualplane(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice = pd3dDevice;
	m_pVB = NULL;
}

CVirtualplane::~CVirtualplane()
{
	SAFE_RELEASE(m_pd3dDevice);
	SAFE_RELEASE(m_pVB);
}

HRESULT CVirtualplane::Initailize()
{
	if(FAILED(InitVB()))			{ return E_FAIL; }

	return S_OK;
}

HRESULT CVirtualplane::InitVB()
{
	/* --------------------------------------------------------------------------------------------------------------------------------
	 * 가상 평면 정점 버퍼 구성하기
	 * -------------------------------------------------------------------------------------------------------------------------------*/
	// 단계1 : 정점에대한정보를정의함
	VIRTUALPLANEVERTEX vertices[] = {
		{ -100.0f,			0.0f,				 0.0f,	0xff00ff00,	},
		{  100.0f,			0.0f,				 0.0f,	0xff0000ff,	},
		{		0.0f,			0.0f,		  -100.0f,	0xffffff00,		},
		{		0.0f,			0.0f,			100.0f,	0xffffff00,		},
		{		0.0f,	-100.0f,					0.0f,	0xffff0000,	},
		{		0.0f,		100.0f,				0.0f,	0xffff0000,	},
	};

	// 단계2 : 정점버퍼를생성하고이에대한포인터를가져옴
	if(FAILED(m_pd3dDevice->CreateVertexBuffer(6*sizeof(VIRTUALPLANEVERTEX),  0, D3DFMT_VIRTUALPLANEVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)))
	{
		return E_FAIL;
	}

	// 단계3 : 정점버퍼에대한포인터를가지고현재정의된정점의데이타를버퍼에써넣는다. 
	VOID* pVertices; 
	if(FAILED(m_pVB->Lock(0, sizeof(vertices), (void**)&pVertices, 0)))
		return E_FAIL;
	memcpy(pVertices, vertices, sizeof(vertices));
	m_pVB->Unlock();

	return S_OK;
}

VOID CVirtualplane::Draw()
{
		  /* ---------------------------------------------------------------------------------------------------------------------------
          /* 가상 평면 그리기
          /* -------------------------------------------------------------------------------------------------------------------------*/
		  // 가상 평면 월드 좌표 셋팅
		  D3DXMATRIXA16 matWorld2;
		  m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(VIRTUALPLANEVERTEX));
		  m_pd3dDevice->SetFVF(D3DFMT_VIRTUALPLANEVERTEX);

		  // 가상 평면 그리기
		  for(float x= -100;  x<= 100; x+=1)
		  {
			  D3DXMatrixTranslation(&matWorld2, x, 0.0, 0.0);
			  m_pd3dDevice->SetTransform(D3DTS_WORLD, &matWorld2);
			  m_pd3dDevice->DrawPrimitive(D3DPT_LINELIST, 2, 1);
		  }

		  for(float z= -100; z<=100; z+=1)
		  {
			  D3DXMatrixTranslation(&matWorld2, 0.0, 0.0, z);
			  m_pd3dDevice->SetTransform(D3DTS_WORLD, &matWorld2);
			  m_pd3dDevice->DrawPrimitive(D3DPT_LINELIST, 0, 1);
		  }
}










