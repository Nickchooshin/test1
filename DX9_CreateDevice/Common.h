#ifndef _COMMON_H_
#define _COMMON_H_

 #include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h> 
#include <cstdlib>
#include "stdio.h"

/* ----------------------------------------------------------------------------------------------------------------------------------
 * 전역 변수 & Define
 * --------------------------------------------------------------------------------------------------------------------------------*/

/// 객체 해제
#define SAFE_DELETE(x) if(x != NULL) { delete x; x = NULL; }
#define SAFE_DELETE_ARRAY(x) if(x != NULL) { delete [] x; x = NULL; }
#define SAFE_RELEASE(x) if(x != NULL) { x->Release(); x = NULL; }

#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")
/* ----------------------------------------------------------------------------------------------------------------------------------
 * 어플리케이션 관련 선언
 * --------------------------------------------------------------------------------------------------------------------------------*/

struct POLAR
{
	float x, y, z;
	float angle;		// 각도
	bool radius;		// 반지름
};

#pragma region _(1) 우주선_

// 정점 정보를 담고 있는 구조체
struct SPACESHIPVERTEX
{
	D3DXVECTOR3		p;
    DWORD				color;
};

// 인덱스 정보를 담고 있는 구조체
struct SPACESHIPINDEX
{
	WORD	_0, _1, _2;
};

// 우주선 객체의 로컬 위치
struct SPACESHIPOBJECT
{
	D3DXMATRIX	matLocal;
};



/// 우주선 버텍스의 구조를 표시하기 위한 FVF 값
#define D3DFMT_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE) 

#pragma endregion 

#pragma region _(2) 가상평면_
// 가상 평면 버텍스 타입 구조체 
struct  VIRTUALPLANEVERTEX
{
      float			x, y, z;
      DWORD		color;
};

/// 가상 평면 버텍스의 구조를 표시하기 위한 FVF 값
#define  D3DFMT_VIRTUALPLANEVERTEX ( D3DFVF_XYZ  | D3DFVF_DIFFUSE )

#pragma endregion

#pragma region _(3) 텍스쳐 맵핑_

// Texture 입힐 큐브 정점 구조체 선언
struct TEXTURECUBEVERTEX
{
	FLOAT				x, y, z;
	D3DCOLOR		color;
	FLOAT				tu, tv;
};

// Texture 입힐 큐브 인덱스 구조체 선언
struct TEXTURECUBEINDEX
{
	WORD _0, _1, _2;
};

#define D3DFMT_TEXTURECUBEVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)

#pragma endregion

#pragma region _(4) 빌보드_

struct BILLBOARDVERTEX
{
	FLOAT x, y, z;
	FLOAT tu, tv;
};

struct BILLBOARDINDEX
{
	WORD _0, _1, _2;
};

#define D3DFMT_BILLBOARDVERTEX (D3DFVF_XYZ | D3DFVF_TEX1)

#pragma endregion

#pragma region _(5) 지형_
struct TERRAINVERTEX
{
	D3DXVECTOR3	position;
	D3DCOLOR			color;
	FLOAT					tu, tv;
};

#define D3DFMT_TERRAINVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)

#pragma  endregion

#endif




