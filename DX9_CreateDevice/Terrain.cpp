#include "Terrain.h"

CTerrain::CTerrain(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice = pd3dDevice;
	m_pVB = NULL;
	m_pTexture = NULL;
}

CTerrain::~CTerrain()
{
	SAFE_RELEASE(m_pd3dDevice);
	SAFE_RELEASE(m_pVB);
	SAFE_RELEASE(m_pTexture);
}

HRESULT CTerrain::Initailize()
{
	if(FAILED(InitVB()))			{ return E_FAIL; }
	if(FAILED(InitTexture()))	{ return E_FAIL; }

	return S_OK;
}

HRESULT CTerrain::InitTexture()
{
	if(FAILED(D3DXCreateTextureFromFile(m_pd3dDevice, "./Resource/seafloor.bmp", &m_pTexture))) {
		MessageBox(NULL, "이미지 파일 없음", "실패", MB_OK);
		return E_FAIL;
	}
	return S_OK;
}

HRESULT CTerrain::InitVB()
{
	TERRAINVERTEX vertices[4]; 
		
		vertices[0].position	= D3DXVECTOR3(-200, 0, 200);
		vertices[0].color			= 0xffffffff;
		vertices[0].tu				= 0.0f;
		vertices[0].tv				= 0.0f;

		vertices[1].position	= D3DXVECTOR3(200, 0, 200);
		vertices[1].color			= 0xffffffff;
		vertices[1].tu				= 10.0f;
		vertices[1].tv				= 0.0f;

		vertices[2].position	= D3DXVECTOR3(-200, 0, -200);
		vertices[2].color			= 0xffffffff;
		vertices[2].tu				= 0.0f;
		vertices[2].tv				= 10.0f;

		vertices[3].position	= D3DXVECTOR3(200, 0, -200);
		vertices[3].color			= 0xffffffff;
		vertices[3].tu				= 10.0f;
		vertices[3].tv				= 10.0f;
	

	if(FAILED(m_pd3dDevice->CreateVertexBuffer(4*sizeof(TEXTURECUBEVERTEX), 0, D3DFMT_TERRAINVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)))
	{
		return E_FAIL;
	}

	VOID* pVertices;
	if(FAILED(m_pVB->Lock(0, sizeof(vertices), (void**)&pVertices, 0)))
		return E_FAIL;
	memcpy(pVertices, vertices, sizeof(vertices));
	m_pVB->Unlock();

	return S_OK;
}

VOID CTerrain::Draw()
{
	// 라이팅 설정
	m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

	// 텍스쳐 설정
	m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,		D3DTOP_MODULATE);
	m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1,	D3DTA_TEXTURE);
	m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2,	D3DTA_DIFFUSE);

	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER,	D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER,	D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MIPFILTER,	D3DTEXF_LINEAR);

	// 월드 매트릭스 초기화
	D3DXMATRIXA16	matWorld;
	D3DXMatrixIdentity( &matWorld );
	m_pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );

	// 그리기 
	m_pd3dDevice->SetTexture(0, m_pTexture);															// 바닥 텍스쳐 설정
	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof( TERRAINVERTEX ) );	// 출력할 버텍스 버퍼 설정
	m_pd3dDevice->SetFVF(D3DFMT_TERRAINVERTEX);												// FVF 값 설정
	m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);							// 삼각형 2개를 이용하여 사각형 영역을 만듬
	m_pd3dDevice->SetTexture(0, NULL);
}

