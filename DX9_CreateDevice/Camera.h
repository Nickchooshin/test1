#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "Common.h"

class CCamera
{
private:
	// 카메라 모드
	enum CAMERA_MODE
	{
		CM_DEFAULT					= 1,		// 기본 모드
		CM_TOPVIEW					= 2,		// 탑뷰
		CM_POLAR						= 3,
	};

	LPDIRECT3DDEVICE9	m_pd3dDevice;		// 카메라 클래스 내부의 디바이스

	// 카메라 관련 벡터
	D3DXVECTOR3				m_vPos;						// 위치
	D3DXVECTOR3				m_vLook;					// 바라보는 곳
	D3DXVECTOR3				m_vUp;						// 상방 벡터	

	// 카메라 관련 메트릭스
	D3DXMATRIXA16			m_matView;			// 뷰 메트릭스
	D3DXMATRIXA16			m_matProjection;	// 프로젝션 메트릭스

	// 카메라 모드
	CAMERA_MODE			m_eCameraMode;	

public:
	// 극 좌표계 모드 스트럭쳐 선언
	POLAR								m_sPolar;			// { 0, 3, -180, 90, 1.0 };

	// 빌보드용 메트릭스
	D3DXMATRIXA16			m_matBillboard;
	D3DXVECTOR3				m_vDir;

public:
	CCamera(LPDIRECT3DDEVICE9 pe3dDevice);
	~CCamera();

	BOOL							InitViewMatrix();								// 뷰 메트릭스 생성
	BOOL							InitProjectionMatrix();						// 프로젝션 메트릭스 생성
	VOID							UpdateCamera();								// 카메라 업데이트
	D3DXMATRIXA16	GetBillBoardMatrix( );						// 빌보드 매트릭스 반환
	D3DXMATRIXA16 GetViewMatrix();
};

#endif // _CAMERA_H_









