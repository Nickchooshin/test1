//-----------------------------------------------------------------------------
// File:	trackball.h
//
// Desc:	simple interface to the SGI trackball code
//
// Last modification: November 16, 2001
//
// Credits: 
// Based on source provided by Bart Sekura
//
// Copyright (c) 2001 - 2002 wolf@direct3d.net All rights reserved.
//-----------------------------------------------------------------------------
#ifndef __trackball_h__
#define __trackball_h__

#include "SGITrackball.h"


#include <D3DX9.h>
#include <d3dx9math.h>

class trackball_t 
{
    float fCurrQuat[4];
    float fLastQuat[4];
    int iBeginX;
    int iBeginY;
    int iWidth;
    int iHeight;

public:    
	trackball_t() {}

	//-----------------------------------------------------------------------------
	// Name: Init()
	// Desc: Initialize the trackball 
	//-----------------------------------------------------------------------------
    VOID Init(int iW, int iH) 
	{
        iWidth = iW,
		iHeight = iH;

        trackball(fCurrQuat,0,0,0,0);
    }

	//-----------------------------------------------------------------------------
	// Name: GetMatrix()
	// Desc: Provides the world matrix to the trackball
	//-----------------------------------------------------------------------------
	VOID SetMatrix(D3DXMATRIX& matReturn) 
	{
		float matTemp[4][4];
        build_rotmatrix(matTemp,fCurrQuat);
        matReturn = &matTemp[0][0];
    }

	//-----------------------------------------------------------------------------
	// Name: TrackMouse()
	// Desc: tracks the mouse and rotates/zooms the object
	//-----------------------------------------------------------------------------
	VOID TrackMouse(HWND m_hWnd, BOOL bZoomDrag, FLOAT* pfZoom)
	{
		static BOOL bMoving = FALSE;
		const WORD wScale = 3;

		if(GetCapture()) 
		{
		    POINT pTrackPoint;
			GetCursorPos(&pTrackPoint);
			ScreenToClient(m_hWnd, &pTrackPoint);

		    int iX = pTrackPoint.x * wScale;
		    int iY = pTrackPoint.y * wScale;

		    if(!bMoving) 
			{
				bMoving = TRUE;
		        trackball(fLastQuat,0,0,0,0);
				iBeginX=iX;
				iBeginY=iY;
			}
			else
			{ 
		        if(iBeginX!=iX||iBeginY!=iY) 
				{
				    float p1x = (FLOAT)(iWidth - 2.0 * iWidth/2.0) / iWidth;
					float p1y = (FLOAT)(2.0 * iHeight/2.0 - iHeight)/iHeight;
					float p2x = (FLOAT)(iWidth - 2.0 * (iX + (- iBeginX + iWidth/2.0f)))/iWidth;
					float p2y = (FLOAT)(2.0 * (iY + (- iBeginY + iHeight/2.0)) - iHeight)/iHeight;
					trackball(fLastQuat, p1x, p1y, p2x, p2y);
					iBeginX = iX;
					iBeginY = iY;
					add_quats(fLastQuat, fCurrQuat, fCurrQuat);
				}
			}
		}
		else 
			bMoving = FALSE;

		// see if we're zooming
		static int dwLastZoom;
		static BOOL bZooming = FALSE;
		if(bZoomDrag) 
		{
			POINT pDragPoint;
			GetCursorPos(&pDragPoint);
			ScreenToClient(m_hWnd, &pDragPoint);
			if(!bZooming) 
			{
				bZooming = TRUE;
				dwLastZoom = pDragPoint.y;
			}
			else 
			{
				*pfZoom += (FLOAT) (pDragPoint.y- dwLastZoom);
				dwLastZoom = pDragPoint.y;
			}
		}
		else 
			bZooming = FALSE;
	}
};

#endif // __trackball_h__