#include "Heightmap.h"

CHeightmap::CHeightmap(LPDIRECT3DDEVICE9 pd3dDevice) : m_pVB(NULL),
													  m_pTexHeight(NULL),
													  m_pTexDiffuse(NULL),
													  m_cxHeight(0),
													  m_czHeight(0)
{
	m_pd3dDevice = pd3dDevice ;

	InitGeometry() ;
}
CHeightmap::~CHeightmap()
{
}

HRESULT CHeightmap::InitTexture()
{
	// 높이맵 텍스처
    if( FAILED( D3DXCreateTextureFromFileEx(m_pd3dDevice,
                    "./Resource/map128.bmp", 
                    D3DX_DEFAULT,
                    D3DX_DEFAULT, 
                    D3DX_DEFAULT,
                    0,
                    D3DFMT_X8R8G8B8,
                    D3DPOOL_MANAGED,
                    D3DX_DEFAULT,
                    D3DX_DEFAULT,
                    0,
                    NULL,
                    NULL, 
                    &m_pTexHeight) ) )
        return E_FAIL;
 
    // 색깔맵 텍스처
    if( FAILED( D3DXCreateTextureFromFile( m_pd3dDevice, "./Resource/tile2.tga", &m_pTexDiffuse) ) )
        return E_FAIL;
 
    m_pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    m_pd3dDevice->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
    m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
    m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
    m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
 
    return S_OK;
}

HRESULT CHeightmap::InitVB()
{
	D3DSURFACE_DESC ddsd ;
	D3DLOCKED_RECT d3drc ;
	m_pTexHeight->GetLevelDesc(0, &ddsd) ; // 텍스쳐의 정보
	m_cxHeight = ddsd.Width ; // 텍스쳐의 가로크기
	m_czHeight = ddsd.Height ; // 텍스쳐의 세로크기

	if( FAILED(m_pd3dDevice->CreateVertexBuffer(ddsd.Width * ddsd.Height * sizeof(HEIGHTMAPVERTEX), 0, D3DFVF_HEIGHTMAPVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)) )
		return E_FAIL ;

	// 텍스쳐의 메모리 Lock
	m_pTexHeight->LockRect(0, &d3drc, NULL, D3DLOCK_READONLY) ;
	void *pVertices ;

	// 정점 버퍼 Lock
	if( FAILED(m_pVB->Lock(0, m_cxHeight * m_czHeight * sizeof(HEIGHTMAPVERTEX), (void**)&pVertices, 0)) )
		return E_FAIL ;

	HEIGHTMAPVERTEX v ;
	HEIGHTMAPVERTEX *pV = (HEIGHTMAPVERTEX*)pVertices ;

	// 좌측 상단 삼각형 + 우측 하단 삼각형 순서로 인덱스 설정
	for(DWORD z=0; z<m_czHeight; z++)
	{
		for(DWORD x=0; x<m_czHeight; x++)
		{
			v.p.x = (float)x-m_cxHeight/2.0f ;
			v.p.z = (float)z-m_czHeight/2.0f ;
			// *((LPDWORD)d3drc.pBits+x+z*(d3drc.Pitch/4)) : x, z 값의 증가에 따른 픽셀 값을 얻어옴
			// 0x000000ff : 블루 값을 높이 정보로 활용함(회색은 R, G, B 의 값이 모두 같음)
			// 10.0f : 높이 보정
			v.p.y = ((float)(*((LPDWORD)d3drc.pBits+x+z*(d3drc.Pitch/4))&0x000000ff))/10.0f ;
			// DWORD이므로 pitch/4
			v.n.x = v.p.x ;
			v.n.y = v.p.y ;
			v.n.z = v.p.z ;
			D3DXVec3Normalize(&v.n, &v.n) ;
			v.t.x = (float)x / (m_cxHeight-1) ;
			v.t.y = (float)z / (m_czHeight-1) ;
			*pV++ = v ;
		}
	}

	m_pVB->Unlock() ;
	m_pTexHeight->UnlockRect(0) ;

	return S_OK ;
}

HRESULT CHeightmap::InitIB()
{
	if( FAILED(m_pd3dDevice->CreateIndexBuffer((m_cxHeight-1)*(m_czHeight-1)*2 * sizeof(HEIGHTMAPVERTEX), 0, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_pIB, NULL)) )
		return E_FAIL ;

	HEIGHTMAPINDEX i ;
	HEIGHTMAPINDEX *pI ;

	if( FAILED(m_pIB->Lock(0, (m_cxHeight-1)*(m_czHeight-1)*2 * sizeof(HEIGHTMAPVERTEX), (void**)&pI, 0)) )
		return E_FAIL ;

	for(DWORD z=0; z<m_czHeight-1; z++)
	{
		for(DWORD x=0; x<m_czHeight-1; x++)
		{
			i._0 = (z*m_cxHeight+x) ;
			i._1 = (z*m_cxHeight+x+1) ;
			i._2 = ((z+1)*m_cxHeight+x) ;
			*pI++ = i ;
			i._0 = ((z+1)*m_cxHeight+x) ;
			i._1 = (z*m_cxHeight+x+1) ;
			i._2 = ((z+1)*m_cxHeight+x+1) ;
			*pI++ = i ;
		}
	}

	m_pIB->Unlock() ;

	return S_OK ;
}

HRESULT CHeightmap::InitGeometry()
{
	//InitMatrices();
    InitTexture();
    InitVB();
    InitIB();

	return S_OK ;
}

void CHeightmap::Draw()
{
    //타겟, Z버퍼를 클리어 시켜 준다. ( 모든 버퍼 초기화 )

    //InputProcess();
    SetupLights();
	
	m_pd3dDevice->SetTexture(0, m_pTexDiffuse);
	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(HEIGHTMAPVERTEX));
	m_pd3dDevice->SetFVF(D3DFVF_HEIGHTMAPVERTEX);
	m_pd3dDevice->SetIndices(m_pIB);
	m_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_cxHeight*m_czHeight, 0, (m_cxHeight-1)*(m_czHeight-1)*2);
	m_pd3dDevice->EndScene();
}

//

void CHeightmap::SetupLights()
{
    D3DMATERIAL9 mtrl;
    ZeroMemory(&mtrl, sizeof(D3DMATERIAL9));
    mtrl.Diffuse  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f); // r, g, b, a
    mtrl.Ambient  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
    mtrl.Specular = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f); 
    mtrl.Emissive = D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);
    mtrl.Power    = 8.0f;  
    m_pd3dDevice->SetMaterial(&mtrl);
 
    // 광원 설정 // -----------------------------------------------------------------------------------
    D3DXVECTOR3 vecDir; // 방향성 광원이 향할 빛의 방향
    D3DLIGHT9 light; // 광원 구조체
    ZeroMemory(&light, sizeof(D3DLIGHT9));
    light.Type = D3DLIGHT_DIRECTIONAL;  // 광원의 종류(방향성)
    light.Diffuse.r = 1.0f;
    light.Diffuse.g = 1.0f;
    light.Diffuse.b = 0.0f;
    light.Ambient.r = 1.0f;
    light.Ambient.g = 1.0f;
    light.Ambient.b = 1.0f;
    light.Specular.r = 1.0f;
    light.Specular.g = 1.0f;
    light.Specular.b = 1.0f;
 
    vecDir = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
    light.Direction = D3DXVECTOR3(cosf(GetTickCount()/350.0f), 1.0f, sinf(GetTickCount()/350.0f));
    D3DXVec3Normalize((D3DXVECTOR3*)&light.Direction, &vecDir);
    light.Range = 1000.0f;
    m_pd3dDevice->SetLight(0, &light);
    m_pd3dDevice->LightEnable(0, TRUE);
    m_pd3dDevice->SetRenderState(D3DRS_AMBIENT, 0x00909090);
} 