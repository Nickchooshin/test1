#include "Bullet.h"

CBullet::CBullet(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice = pd3dDevice ;
	m_pVB = NULL ;
	m_pTexture = NULL ;

	m_bState = false ;
	m_fX = 0.0f ;
	m_fY = 0.0f ;
	m_fZ = 0.0f ;
	m_fDeltaX = 0.0f ;
	m_fDeltaZ = 0.0f ;

	Initialize() ;
}
CBullet::~CBullet()
{
	SAFE_RELEASE(m_pd3dDevice) ;
	SAFE_RELEASE(m_pVB) ;
	SAFE_RELEASE(m_pTexture) ;
}

HRESULT CBullet::Initialize()
{
	if(FAILED(InitVB())) { return E_FAIL ; }
	if(FAILED(InitTexture())) { return E_FAIL ; }

	return S_OK ;
}

HRESULT CBullet::InitVB()
{
	BULLETVERTEX vertices[4] ;

	vertices[0].position = D3DXVECTOR3(-0.5, 2, 0) ;
	vertices[0].color = 0xffffffff ;
	vertices[0].tu = 0.0f ;
	vertices[0].tv = 0.0f ;

	vertices[1].position = D3DXVECTOR3(0.5, 2, 0) ;
	vertices[1].color = 0xffffffff ;
	vertices[1].tu = 1.0f ;
	vertices[1].tv = 0.0f ;

	vertices[2].position = D3DXVECTOR3(-0.5, 1, 0) ;
	vertices[2].color = 0xffffffff ;
	vertices[2].tu = 0.0f ;
	vertices[2].tv = 1.0f ;

	vertices[3].position = D3DXVECTOR3(0.5, 1, 0) ;
	vertices[3].color = 0xffffffff ;
	vertices[3].tu = 1.0f ;
	vertices[3].tv = 1.0f ;

	if(FAILED(m_pd3dDevice->CreateVertexBuffer(4*sizeof(BULLETVERTEX), 0, D3DFMT_BULLETVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL)))
	{
		return E_FAIL ;
	}

	void *pVertices ;
	if(FAILED(m_pVB->Lock(0, sizeof(vertices), (void**)&pVertices, 0)))
		return E_FAIL ;
	memcpy(pVertices, vertices, sizeof(vertices)) ;
	m_pVB->Unlock() ;

	return S_OK ;
}

HRESULT CBullet::InitTexture()
{
	if(FAILED(D3DXCreateTextureFromFile(m_pd3dDevice, "./Resource/bullet.dds", &m_pTexture)))
	{
		MessageBox(NULL, "이미지 파일 없음", "실패", MB_OK) ;
		return E_FAIL ;
	}

	return S_OK ;
}

void CBullet::Draw()
{
	m_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, true) ;
	m_pd3dDevice->SetRenderState(D3DRS_ALPHAREF, 0x08) ;
	m_pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL) ;

	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(BULLETVERTEX)) ;	// 버텍스 데이터
	m_pd3dDevice->SetFVF(D3DFMT_BULLETVERTEX) ;	// 버텍스 정보
	m_pd3dDevice->SetTexture(0, m_pTexture) ;	// 텍스쳐 정보
	m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2) ;	// fpsejfld

	BulletControl() ;
}

void CBullet::BulletControl()
{
	if(m_bState==false)
		return ;

	m_fX += m_fDeltaX ;
	m_fZ += m_fDeltaZ ;

	// 게임 세계를 벗어나면 총알 소거 및 재사용이 가능하도록 상태 설정
	if(m_fX<=-200 || m_fX>=200)
	{
		m_bState = false ;
		return ;
	}

	if(m_fZ<=-200 || m_fZ>=200)
	{
		m_bState = false ;
		return ;
	}

	// 충돌체크
	//float distance = (float) sqrt( (g_Bullet.x - g_TankX) * (g_Bullet.x - g_TankX) + (g_Bullet.z - g_TankZ) * (g_Bullet.z - g_TankZ)) ;

	// 총알과 탱크의 거리 계산

	//if(distance<40)
	//{
	//	g_Bullet.state = false ;
	//}
}

bool CBullet::CrashCheck(float TankX, float TankZ)
{
	// 충돌체크
	float distance = (float) sqrt( (m_fX - TankX) * (m_fX - TankX) + (m_fZ - TankZ) * (m_fZ - TankZ)) ;

	// 총알과 탱크의 거리 계산

	if(distance<40)
	{
		m_bState = false ;
		return true ;
	}

	return false ;
}