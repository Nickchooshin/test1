#ifndef  _TERRAIN_H_
#define _TERRAIN_H_

#include "Common.h"

class CTerrain
{
private:
	LPDIRECT3DDEVICE9						m_pd3dDevice;		// 렌더링에 사용될 D3D 디바이스
	LPDIRECT3DVERTEXBUFFER9			m_pVB;						// 정점을 보관할 정점 버퍼 
	LPDIRECT3DTEXTURE9						m_pTexture;			// 텍스쳐

public:														
	CTerrain(LPDIRECT3DDEVICE9 pd3dDevice);					// 오버로딩 생성자
	~CTerrain();																				// 소멸자

	HRESULT Initailize();
	HRESULT InitVB();
	HRESULT InitTexture();

	VOID Draw();
};

#endif










