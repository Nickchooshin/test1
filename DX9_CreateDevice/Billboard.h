#ifndef _BILLBOARD_H_
#define _BILLBOARD_H_

#include "Common.h"

class CBillboard
{
private:
	LPDIRECT3DDEVICE9						m_pd3dDevice;		// 렌더링에 사용될 D3D 디바이스
	LPDIRECT3DVERTEXBUFFER9			m_pVB;						// 정점을 보관할 정점 버퍼 
	LPDIRECT3DINDEXBUFFER9			m_pIB;						// 정점 인덱스를 보관할 버퍼
	LPDIRECT3DTEXTURE9						m_pTexture;			// 텍스쳐

public:														
	CBillboard(LPDIRECT3DDEVICE9 pd3dDevice);				// 오버로딩 생성자
	~CBillboard();																			// 소멸자

	HRESULT Initailize();
	HRESULT InitVB();
	HRESULT InitIB();
	HRESULT InitTexture();

	VOID Draw();
};

#endif 