#include "Camera.h"

CCamera::CCamera(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice = pd3dDevice;
	m_eCameraMode = CM_POLAR;

	// 극좌표계 초기화
	m_sPolar.x = 0;
	m_sPolar.y = 10;
	m_sPolar.z = -180;
	m_sPolar.angle = 90;
	m_sPolar.radius = 1.0;	// 한스템에 이동하는 거리

	InitViewMatrix();
	InitProjectionMatrix();
}

CCamera::~CCamera()
{
}

// 최초 카메라 정보를 설정한다. 
BOOL CCamera::InitViewMatrix()
{
	switch(m_eCameraMode)
	{
	case CM_DEFAULT:
		m_vPos	= D3DXVECTOR3(70.0f, 30.0f, -180.0f);
		m_vLook	= D3DXVECTOR3(0.0f, 0.0f,  0.0f);
		m_vUp	= D3DXVECTOR3(0.0f, 1.0f,  0.0f);

		D3DXMatrixLookAtLH ( &m_matView, &m_vPos, &m_vLook, &m_vUp );	// 뷰 행렬 생성
		m_pd3dDevice->SetTransform ( D3DTS_VIEW, &m_matView );						// 뷰 행렬 등록
		break;

	case CM_TOPVIEW:
		break;

	case CM_POLAR:
		// 카메라가 바라보는 위치 구하기 (평면 움직임이므로 y 좌표는 그대로 사용됨)
		float destX = (float) ( m_sPolar.x + m_sPolar.radius * cos(m_sPolar.angle * (D3DX_PI / 180.0f ) ) );
		float destZ = (float) ( m_sPolar.z + m_sPolar.radius * sin(m_sPolar.angle * (D3DX_PI / 180.0f ) ) );

		m_vPos		= D3DXVECTOR3 ( m_sPolar.x, m_sPolar.y, m_sPolar.z );				// 카메라의 위치
		m_vLook	= D3DXVECTOR3 ( destX, m_sPolar.y, destZ );									// 카메라가 바라보는 지점
		m_vUp		= D3DXVECTOR3 ( 0.0f, 1.0f, 0.0f );													// 업벡터 설정

		D3DXMatrixLookAtLH ( &m_matView, &m_vPos, &m_vLook, &m_vUp );	// 뷰 행렬 생성
		m_pd3dDevice->SetTransform ( D3DTS_VIEW, &m_matView );						// 뷰 행렬 등록

		// 빌보드 매트릭스 설정
		/*
		D3DXVECTOR3 vDir = m_vLook - m_vPos;
		if ( vDir.x > 0.0f )
			D3DXMatrixRotationY( &m_matBillboard, -atanf( vDir.z / vDir.x) + D3DX_PI / 2 );
		else 
			D3DXMatrixRotationY( &m_matBillboard, -atanf( vDir.z / vDir.x) - D3DX_PI / 2 );

		m_vDir = vDir;
		*/

		D3DXMatrixIdentity(&m_matBillboard);
		m_matBillboard._11 = m_matView._11;
		m_matBillboard._13 = m_matView._13;
		m_matBillboard._31 = m_matView._31;
		m_matBillboard._33 = m_matView._33;
		D3DXMatrixInverse(&m_matBillboard, NULL, &m_matBillboard);

		break;
	}
	return TRUE;
}

BOOL CCamera::InitProjectionMatrix()
{
	D3DXMatrixPerspectiveFovLH(&m_matProjection, D3DX_PI/4, 1.0f / 1.0f, 1.0f, 1000.0f);// 프로젝션 행렬 생성
	m_pd3dDevice->SetTransform(D3DTS_PROJECTION, &m_matProjection);								// 프로젝션 행렬 등록

	return TRUE;
}

VOID CCamera::UpdateCamera()
{
	InitViewMatrix();
	InitProjectionMatrix();
}

D3DXMATRIXA16 CCamera::GetBillBoardMatrix( )
{
	return m_matBillboard;
}

D3DXMATRIXA16 CCamera::GetViewMatrix()
{
	return m_matView;
}
