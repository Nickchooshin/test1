#include "Spaceship.h"

CSpaceship::CSpaceship(LPDIRECT3DDEVICE9 pd3dDevice)
{
	m_pd3dDevice	= pd3dDevice;
	m_pVB					= NULL;
	m_pIB					= NULL;
}

CSpaceship::~CSpaceship()
{
	SAFE_RELEASE(m_pd3dDevice);
	SAFE_RELEASE(m_pVB);
	SAFE_RELEASE(m_pIB);
}

HRESULT CSpaceship::Initailize()
{
	if(FAILED(InitVB()))			{ return E_FAIL; }
	if(FAILED(InitIB()))				{ return E_FAIL; }

	return S_OK;
}

HRESULT CSpaceship::InitVB()
{
	/* --------------------------------------------------------------------------------------------------------------------------------
	 * 우주선 정점 버퍼 구성하기
	 * -------------------------------------------------------------------------------------------------------------------------------*/
	// object geometry를 구성함(vertex의 위치를 지정함) 
	D3DXVECTOR3 p1 = D3DXVECTOR3( 0.00f, 0.00f, 0.50f );  
	D3DXVECTOR3 p2 = D3DXVECTOR3( 0.50f, 0.00f,-0.50f );
	D3DXVECTOR3 p3 = D3DXVECTOR3( 0.15f, 0.15f,-0.35f );
	D3DXVECTOR3 p4 = D3DXVECTOR3(-0.15f, 0.15f,-0.35f );
	D3DXVECTOR3 p5 = D3DXVECTOR3( 0.15f,-0.15f,-0.35f );
	D3DXVECTOR3 p6 = D3DXVECTOR3(-0.15f,-0.15f,-0.35f );
	D3DXVECTOR3 p7 = D3DXVECTOR3(-0.50f, 0.00f,-0.50f );

	// Vertex들을 우주선의 모양에 따라 재 배열함 
	// 윗면들 정의(우주선의 위) : 3개의 삼각형으로 구성됨 - 인덱스 면 (1, 2, 3)
	m_pVertices[0].p = p1; m_pVertices[0].color = 0xffebb1b;
	m_pVertices[1].p = p2; m_pVertices[1].color = 0xffebb1b; 
	m_pVertices[2].p = p3; m_pVertices[2].color = 0xffebb1b;
	m_pVertices[3].p = p4; m_pVertices[3].color = 0xffebb1b;
	m_pVertices[4].p = p7; m_pVertices[4].color = 0xffebb1b;

	// 밑에 면들 정의(우주선의 바닥) : 3개의 삼각형으로 구성됨 - 인덱스 면 (4, 5, 6)
	m_pVertices[5].p = p1; m_pVertices[5].color = 0xffebb1b;
	m_pVertices[6].p = p2; m_pVertices[6].color = 0xffebb1b;
	m_pVertices[7].p = p5; m_pVertices[7].color = 0xffebb1b;
	m_pVertices[8].p = p6; m_pVertices[8].color = 0xffebb1b;
	m_pVertices[9].p = p7; m_pVertices[9].color = 0xffebb1b;
 
	// 뒷면들 정의(우주선의 뒤) : 4개의 삼각형으로 구성됨 - 인덱스 면 (7, 8, 9, 10)
	m_pVertices[10].p = p2; m_pVertices[10].color = 0xffebb1b;
	m_pVertices[11].p = p3; m_pVertices[11].color = 0xffebb1b;
	m_pVertices[12].p = p4; m_pVertices[12].color = 0xffebb1b;
	m_pVertices[13].p = p7; m_pVertices[13].color = 0xffebb1b;
	m_pVertices[14].p = p6; m_pVertices[14].color = 0xffebb1b;
	m_pVertices[15].p = p5; m_pVertices[15].color = 0xffebb1b;

	// 우주선을 위한 정점 버퍼를 생성하고 이에 대한 포인터를 가져옴
    if(FAILED(m_pd3dDevice->CreateVertexBuffer(sizeof(m_pVertices), 0, D3DFMT_CUSTOMVERTEX, D3DPOOL_DEFAULT, &m_pVB, NULL))) {
        return E_FAIL;
    }
    // 그래픽 카드안에 있는 정점 버퍼에 대한 포인터를 가지고 현재 정의된 정점의 데이타를 버퍼에 써 넣는다. 
    VOID* pVertices; 
	if(FAILED(m_pVB->Lock(0, sizeof(m_pVertices), (void**)&pVertices, 0)))
        return E_FAIL; 
    memcpy(pVertices, m_pVertices, sizeof(m_pVertices)); 
    m_pVB->Unlock();

	return S_OK;
}

HRESULT CSpaceship::InitIB()
{
	/* --------------------------------------------------------------------------------------------------------------------------------
	 * 우주선 인데스 버퍼 구성하기
	 * -------------------------------------------------------------------------------------------------------------------------------*/
    // 각 면에 대한 vertex의 인덱스를  설정함(10개의 면)
    m_pIndices[0]._0 = 0;   /*p1*/	m_pIndices[0]._1 = 1;   /*p2*/	m_pIndices[0]._2 = 2;   /*p3*/		// Triangle 1		(Top - Right)
    m_pIndices[1]._0 = 0;   /*p1*/	m_pIndices[1]._1 = 2;   /*p3*/	m_pIndices[1]._2 = 3;   /*p4*/		// Triangle 2		(Top - Middle)
    m_pIndices[2]._0 = 0;   /*p1*/	m_pIndices[2]._1 = 3;   /*p4*/	m_pIndices[2]._2 = 4;   /*p7*/		// Triangle 3		(Top - Left)
    m_pIndices[3]._0 = 5;   /*p1*/	m_pIndices[3]._1 = 7;   /*p5*/	m_pIndices[3]._2 = 6;   /*p2*/		// Triangle 4		(Bottom - Right)
    m_pIndices[4]._0 = 5;   /*p1*/	m_pIndices[4]._1 = 8;   /*p6*/	m_pIndices[4]._2 = 7;   /*p5*/		// Triangle 5		(Bottom - Middle)
    m_pIndices[5]._0 = 5;   /*p1*/	m_pIndices[5]._1 = 9;   /*p2*/	m_pIndices[5]._2 = 8;   /*p6*/		// Triangle 6		(Bottom - Left)
    m_pIndices[6]._0 = 10; /*p2*/	m_pIndices[6]._1 = 15; /*p5*/	m_pIndices[6]._2 = 11; /*p3*/		// Triangle 7		(Rear - Right) 
    m_pIndices[7]._0 = 11; /*p3*/	m_pIndices[7]._1 = 15; /*p5*/	m_pIndices[7]._2 = 12; /*p4*/		// Triangle 8		(Rear - Middle 1)
    m_pIndices[8]._0 = 12; /*p4*/	m_pIndices[8]._1 = 15; /*p5*/	m_pIndices[8]._2 = 14; /*p6*/		// Triangle 9		(Rear - Middle 2) 
    m_pIndices[9]._0 = 12; /*p4*/	m_pIndices[9]._1 = 14; /*p6*/	m_pIndices[9]._2 = 13; /*p7*/		// Triangle 10		(Rear - Left)

    if(FAILED(m_pd3dDevice->CreateIndexBuffer(sizeof(m_pIndices), 0, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_pIB, NULL))) {
        return E_FAIL;
    }

    VOID* pIndeces;
    if(FAILED(m_pIB->Lock(0, sizeof(m_pIndices), (void**)&pIndeces, 0)))
        return E_FAIL;
    memcpy(pIndeces, m_pIndices, sizeof(m_pIndices));
    m_pIB->Unlock();

	return S_OK;
}

VOID CSpaceship::Draw()
{
	// 인덱스가 포함된 사용자 정의 우주선 그리기
	m_pd3dDevice->SetStreamSource(0, m_pVB, 0, sizeof(SPACESHIPVERTEX));
	m_pd3dDevice->SetFVF(D3DFMT_CUSTOMVERTEX);
	m_pd3dDevice->SetIndices(m_pIB);
	m_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 16, 0, 10);
}










